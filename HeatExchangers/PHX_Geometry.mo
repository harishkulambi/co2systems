within HeatExchangers;

model PHX_Geometry
  import SI = Modelica.SIunits;
  parameter String fluid1 = "carbondioxide";
  parameter String fluid2 = "water";
  CoolPropLinker.Interfaces.Functions CP;
  CoolPropLinker.Interfaces.BaseProperties primaryin;
  CoolPropLinker.Interfaces.SpecialProperties splprimaryin;
  CoolPropLinker.Interfaces.BaseProperties secondaryin;
  CoolPropLinker.Interfaces.SpecialProperties splsecondaryin;
  parameter SI.Length Lv = 1.55;
  parameter SI.Length Lh = 0.43;
  parameter SI.Length Lc = 0.38;
  parameter SI.Thickness t = 0.6e-3;
  parameter SI.Diameter Dp = 200e-3;
  parameter SI.Area A = 110;
  parameter SI.ThermalConductivity K = 17.5;
  parameter Real SEF1 = 1.25;
  parameter Real Np = 1;
  parameter SI.Volume V = 1 / n;
  //SI.Volume V1[n] = V/n;
  SI.Area A_projected[n];
  SI.Area A_developed[n];
  SI.Length Lp[n], Lw;
  Real N_effective[n];
  Real Nt[n];
  Real Ncp[n];
  SI.Breadth b[n];
  Real P[n];
  SI.Diameter Dh[n];
  SI.Area Ach[n];
  parameter Integer n = 10;
  SI.Length Lv1;
  Real SEF;
  Real A1;
  parameter SI.MassFlowRate min_primary = 2 ;
  parameter SI.MassFlowRate min_secondary = 2;
  parameter SI.Pressure Pin_primary = 76e5;
  parameter SI.Temperature Tin_primary = 150 + 273.15;
  parameter SI.Pressure Pin_secondary = 4e5;
  parameter SI.Temperature Tin_secondary = 25 + 273.15;
  //SI.Temperature Tout_primary;
  //SI.Temperature Tout_secondary;
  SI.MassFlowRate mch_primary[n];
  SI.MassFlowRate mch_secondary[n];
  Real G_primary[n], G_secondary[n];
  Real Re_primary, Re_secondary;
  Real Prandtl_avg, viscosity_avg;
  Real f_primary, f_secondary, Nu_primary, Nu_secondary;
  Real h_primary[n], h_secondary[n];
  Real U_clean[n];
  Real U_fouled[n];
  Real Qr[n], Qf[n];
//  SI.Temperature Tout_primary[n](start = 100 + 273.15);
//  SI.Temperature Tout_secondary[n](start = 15 + 273.15);

  SI.Temperature Tout_primary[n];
  SI.Temperature Tout_secondary[n];  
  
  SI.Temperature Tin_primary1[n];
  SI.Temperature Tin_secondary1[n];
  

equation

  Lw = Lh + Dp;
  SEF = SEF1 / n;
  Lv1 = Lv / n;
  A1 = A / n;
  primaryin.state = CP.setProperty(Pin_primary, Tin_primary, fluid1, "pT");
  splprimaryin.state = CP.setProperty(Pin_primary, primaryin.h, fluid1, "SpecialProperties");
  secondaryin.state = CP.setProperty(Pin_secondary, Tin_secondary, fluid2, "pT");
  splsecondaryin.state = CP.setProperty(Pin_secondary, secondaryin.h, fluid2, "SpecialProperties");
  Prandtl_avg = (splprimaryin.Prandtl + splsecondaryin.Prandtl) / 2;
  viscosity_avg = (splprimaryin.viscosity + splsecondaryin.viscosity) / 2;
  
  Tin_primary1[1] = Tout_primary[1];
  Tin_secondary1[1] = Tout_secondary[1];
  
    Lp[1] = abs(Lv1 - Dp);
  A_projected[1] = Lp[1] * Lw;
  A_developed[1] = A1 / N_effective[1];
  Nt[1] = ceil(N_effective[1]) + 2;
  Ncp[1] = Nt[1] - 1 / (2 * Np);
  b[1] = abs(P[1] - t);
  P[1] = Lc / Nt[1];
  Dh[1] = 2 * b[1] / SEF;
  Ach[1] = abs(b[1] * Lw);
  SEF = A_developed[1] / A_projected[1];
  mch_primary[1] = min_primary / Ncp[1];
  mch_secondary[1] = min_secondary / Ncp[1];
  G_primary[1] = mch_primary[1] / Ach[1];
  G_secondary[1] = mch_secondary[1] / Ach[1];
  Nu_primary = h_primary[1] * Dh[1] / splprimaryin.Conductivity;
  Nu_secondary = h_secondary[1] * Dh[1] / splsecondaryin.Conductivity;
  1 / U_clean[1] = 1 / h_primary[1] + 1 / h_secondary[1] + t / K;
  1 / U_fouled[1] = 1 / U_clean[1] + 0.0000827;
  Qr[1] = min_primary * splprimaryin.CpMass * abs(Tin_primary - Tout_primary[1]);
  Qf[1] = U_fouled[1] * (SEF * Lp[1] * Lw * N_effective[1]) * ((Tin_primary - Tout_secondary[1]) / 2 + (Tout_primary[1] - Tin_secondary) / 2);
  
//  min_secondary * splsecondaryin.CpMass * (Tin_secondary - Tout_secondary[1]) + min_primary * splprimaryin.CpMass * (Tin_primary - Tout_primary[1]) = secondaryin.d * V * splsecondaryin.CpMass * der(Tout_secondary[1]);
  
//  min_primary * splprimaryin.CpMass * (Tin_primary - Tout_primary[1]) + min_secondary * splsecondaryin.CpMass * (Tin_secondary - Tout_secondary[1]) = primaryin.d * V * splprimaryin.CpMass * der(Tout_primary[1]);
  
  (min_primary/n)*splprimaryin.CpMass*(Tin_primary-Tout_primary[1]) - (min_secondary/n)*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary[1]) = primaryin.d*V*splprimaryin.CpMass*der(Tout_primary[1]);
      (min_secondary/n)*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary[1])- (min_primary/n)*splprimaryin.CpMass*(Tin_primary-Tout_primary[1]) = secondaryin.d*V*splsecondaryin.CpMass*der(Tout_secondary[1]);
  for i in 2:n - 1 loop
//      Lp[i] = abs(Lv1 - Dp);
//      A_projected[i] = Lp[i]*Lw;
//      A_developed[i] = A1/N_effective[i];
//      Nt[i] = ceil(N_effective[i]) + 2;
//      Ncp[i] = Nt[i]-1/(2*Np);
//      b[i] = abs(P[i] - t);
//      P[i] = Lc/Nt[i];
//      Dh[i] = 2*b[i]/SEF;
//      Ach[i] = abs(b[i]*Lw);
//      SEF= A_developed[i]/A_projected[i];
//      mch_primary[i] = min_primary/Ncp[i];
//      mch_secondary[i] = min_secondary/Ncp[i];
//      G_primary[i] = mch_primary[i]/Ach[i];
//      G_secondary[i] = mch_secondary[i]/Ach[i];
//      Nu_primary = h_primary[i]*Dh[i]/splprimaryin.Conductivity;
//      Nu_secondary = h_secondary[i]*Dh[i]/splsecondaryin.Conductivity;
//      (1/U_clean[i]) = (1/h_primary[i])+(1/h_secondary[i])+(t/K);
//      (1/U_fouled[i]) = (1/U_clean[i])+ 0.0000827;
//      Qr[i] = min_primary*splprimaryin.CpMass*(abs(Tin_primary-Tout_primary[i]));
//      Qf[i] = U_fouled[i]*(SEF*Lp[i]*Lw*N_effective[i])*(((Tin_primary-Tout_secondary[i])/2)+((Tout_primary[i]-Tin_secondary)/2));
//      min_secondary*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary[i]) + min_primary*splprimaryin.CpMass*(Tin_primary-Tout_primary[i]) = secondaryin.d*V*splsecondaryin.CpMass*der(Tout_secondary[i]);
//min_primary*splprimaryin.CpMass*(Tin_primary-Tout_primary[i]) + min_secondary*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary[i]) = primaryin.d*V*splprimaryin.CpMass*der(Tout_primary[i]);
    Lp[i] = abs(Lv1 - Dp);
    A_projected[i] = Lp[i] * Lw;
    A_developed[i] = A1 / N_effective[i];
    Nt[i] = ceil(N_effective[i]) + 2;
    Ncp[i] = Nt[i] - 1 / (2 * Np);
    b[i] = abs(P[i] - t);
    P[i] = Lc / Nt[i];
    Dh[i] = 2 * b[i] / SEF;
    Ach[i] = abs(b[i] * Lw);
    SEF = A_developed[i] / A_projected[i];
    mch_primary[i] = min_primary / Ncp[i];
    mch_secondary[i] = min_secondary / Ncp[i];
    G_primary[i] = mch_primary[i] / Ach[i];
    G_secondary[i] = mch_secondary[i] / Ach[i];
    Nu_primary = h_primary[i] * Dh[i] / splprimaryin.Conductivity;
    Nu_secondary = h_secondary[i] * Dh[i] / splsecondaryin.Conductivity;
    1 / U_clean[i] = 1 / h_primary[i] + 1 / h_secondary[i] + t / K;
    1 / U_fouled[i] = 1 / U_clean[i] + 0.0000827;
    
    
//    Tin_primary1[i] = Tout_primary[i-1];
//    Tin_secondary1[i] = Tout_secondary[i-1];
 Tin_primary1[i] = Tout_primary[i];
    Tin_secondary1[i] = Tout_secondary[i];    
    

    
    Qr[i] = min_primary * splprimaryin.CpMass * abs(Tin_primary1[i-1] - Tout_primary[i]) ;
    
//    Qf[i] = U_fouled[i] * (SEF * Lp[i] * Lw * N_effective[i]) * ((Tin_primary1[i] - Tout_secondary[i]) / 2 + (Tout_primary[i] - Tin_secondary1[i]) / 2) + Qf[i + 1];
    
    Qf[i] = U_fouled[i]*(SEF*Lp[i]*Lw*N_effective[i])*(((Tout_primary[i-1]-Tout_secondary[i])/2)+((Tout_primary[i]-Tout_secondary[i-1])/2));
  
    
//    min_secondary * splsecondaryin.CpMass * (Tin_secondary1[i] - Tout_secondary[i]) + min_primary * splprimaryin.CpMass * (Tin_primary1[i] - Tout_primary[i]) = secondaryin.d * V * splsecondaryin.CpMass * der(Tout_secondary[i]);
    
//    min_primary * splprimaryin.CpMass * (Tin_primary1[i] - Tout_primary[i]) + min_secondary * splsecondaryin.CpMass * (Tin_secondary1[i] - Tout_secondary[i])  = primaryin.d * V * splprimaryin.CpMass * der(Tout_primary[i]);
    
//    min_secondary*splsecondaryin.CpMass*(Tout_secondary[i-1]-Tout_secondary[i]) + min_primary*splprimaryin.CpMass*(Tout_primary[i-1]-Tout_primary[i]) = secondaryin.d*V*splsecondaryin.CpMass*der(Tout_secondary[i]);
    
//    min_primary*splprimaryin.CpMass*(Tout_primary[i-1]-Tout_primary[i]) + min_secondary*splsecondaryin.CpMass*(Tout_secondary[i-1]-Tout_primary[i]) = primaryin.d*V*splprimaryin.CpMass*der(Tout_primary[i]);
    
      min_primary/n*splprimaryin.CpMass*(Tout_primary[i-1]-Tout_primary[i]) = primaryin.d*V*splprimaryin.CpMass*der(Tout_primary[i]);
      min_secondary/n*splsecondaryin.CpMass*(Tout_secondary[i-1]-Tout_secondary[i]) = secondaryin.d*V*splsecondaryin.CpMass*der(Tout_secondary[i]);

  end for;
  

  
  Lp[n] = abs(Lv1 - Dp);
  A_projected[n] = Lp[n] * Lw;
  A_developed[n] = A1 / N_effective[n];
  Nt[n] = ceil(N_effective[n]) + 2;
  Ncp[n] = Nt[n] - 1 / (2 * Np);
  b[n] = abs(P[n] - t);
  P[n] = Lc / Nt[n];
  Dh[n] = 2 * b[n] / SEF;
  Ach[n] = abs(b[n] * Lw);
  SEF = A_developed[n] / A_projected[n];
  mch_primary[n] = min_primary / Ncp[n];
  mch_secondary[n] = min_secondary / Ncp[n];
  G_primary[n] = mch_primary[n] / Ach[n];
  G_secondary[n] = mch_secondary[n] / Ach[n];
  Nu_primary = h_primary[n] * Dh[n] / splprimaryin.Conductivity;
  Nu_secondary = h_secondary[n] * Dh[n] / splsecondaryin.Conductivity;
  1 / U_clean[n] = 1 / h_primary[n] + 1 / h_secondary[n] + t / K;
  1 / U_fouled[n] = 1 / U_clean[n] + 0.0000827;
  Qr[n] = min_primary * splprimaryin.CpMass * abs(Tin_primary - Tout_primary[n]);
  
  Qf[n] = U_fouled[n] * (SEF * Lp[n] * Lw * N_effective[n]) * ((Tin_primary - Tout_secondary[n]) / 2 + (Tout_primary[n] - Tin_secondary) / 2);
  
  min_secondary * splsecondaryin.CpMass * (Tin_secondary1[n] - Tout_secondary[n]) + min_primary * splprimaryin.CpMass * (Tin_primary1[n] - Tout_primary[n]) = secondaryin.d * V * splsecondaryin.CpMass * der(Tout_secondary[n]);
  
  min_primary * splprimaryin.CpMass * (Tin_primary1[n-1] - Tout_primary[n]) + min_secondary * splsecondaryin.CpMass * (Tin_secondary1[n-1] - Tout_secondary[n]) = primaryin.d * V * splprimaryin.CpMass * der(Tout_primary[n]);
  
  Re_secondary = G_secondary * Dh / splsecondaryin.viscosity;
  Re_primary = G_primary * Dh / splprimaryin.viscosity;
  f_primary = 0.085 * exp(1.52 * tan(60)) * Re_primary ^ (-(0.25 - 0.06 * tan(60)));
  Nu_primary = 0.051 * exp(0.64 * tan(60)) * Re_primary ^ 0.73 * splprimaryin.Prandtl ^ 0.43 * (splprimaryin.Prandtl / Prandtl_avg) ^ 0.25;
  f_secondary = 0.085 * exp(1.52 * tan(60)) * Re_secondary ^ (-(0.25 - 0.06 * tan(60)));
  Nu_secondary = 0.051 * exp(0.64 * tan(60)) * Re_secondary ^ 0.73 * splsecondaryin.Prandtl ^ 0.43 * (splsecondaryin.Prandtl / Prandtl_avg) ^ 0.25;
  
  Tin_primary1[n] = Tout_primary[n];
  Tin_secondary1[n] = Tout_secondary[n];
  
end PHX_Geometry;
