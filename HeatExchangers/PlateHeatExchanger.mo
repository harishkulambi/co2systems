within HeatExchangers;

model PlateHeatExchanger
  import SI = Modelica.SIunits;
  
  parameter String fluid1 = "carbondioxide";
  parameter String fluid2 = "water";
  
  CoolPropLinker.Interfaces.Functions CP;
  
  CoolPropLinker.Interfaces.BaseProperties primaryin;
  CoolPropLinker.Interfaces.BaseProperties primaryout;
  CoolPropLinker.Interfaces.SpecialProperties splprimaryin;
  CoolPropLinker.Interfaces.SpecialProperties splprimaryout;
  CoolPropLinker.Interfaces.BaseProperties secondaryin;
  CoolPropLinker.Interfaces.BaseProperties secondaryout;
  CoolPropLinker.Interfaces.SpecialProperties splsecondaryin;
  CoolPropLinker.Interfaces.SpecialProperties splsecondaryout;
  
  
  parameter SI.Length Lv = 1.55;
  parameter SI.Length Lh = 0.43;
  parameter SI.Length Lc = 0.38;
  parameter SI.Thickness t = 0.6e-3;
  parameter SI.Diameter Dp = 200e-3;
  parameter SI.Area A = 110;
  parameter SI.Conductivity K = 17.5;
  parameter Real SEF = 1.25;
  parameter Real Np = 1;
  parameter Real beta = 60;
  parameter SI.Volume V = 1;
  
  SI.Area A_projected;
  SI.Area A_developed;
  
  SI.Length Lp;
  SI.Length Lw;
  Real N_effective;
  Real Nt;
  Real Ncp;
  
  SI.Breadth b;
  Real P;
  SI.Diameter Dh;
  SI.Area Ach;
  
  
  SI.Pressure Pin_primary;
  SI.Temperature Tin_primary;
  SI.MassFlowRate min_primary;
  SI.Temperature Tout_primary(start = 15+273.15);
  
  SI.Pressure Pin_secondary;
  SI.Temperature Tin_secondary;
  SI.MassFlowRate min_secondary;
  SI.Temperature Tout_secondary(start = 15+273.15);
  
  SI.MassFlowRate mch_primary;
  SI.MassFlowRate mch_secondary;
  
  Real G_primary(unit = "kg/sm^2");
  Real G_secondary(unit = "kg/sm^2");
  
  Real Re_primary;
  Real Re_secondary;
  
  Real f_primary;
  Real f_secondary;
  
  Real Nu_primary;
  Real Nu_secondary;
  
  Real Prandtl_avg;
  Real Viscosity_avg;
  
  SI.CoefficientOfHeatTransfer h_primary;
  SI.CoefficientOfHeatTransfer h_secondary;
  SI.CoefficientOfHeatTransfer U_clean;
  SI.CoefficientOfHeatTransfer U_fouled;
  
  Real CF;
  SI.Power Qr;
  SI.Power Qf;
  Real CS;
  
  
  
protected
  MI_ThermoSystems.Interfaces.InletPort coldin annotation(
    Placement(visible = true, transformation(origin = {-110, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort coldout annotation(
    Placement(visible = true, transformation(origin = {110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.InletPort hotin annotation(
    Placement(visible = true, transformation(origin = {110, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort hotout annotation(
    Placement(visible = true, transformation(origin = {-110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
coldin.P = Pin_secondary;
coldin.Q = min_secondary;

coldout.P = Pin_secondary;
coldout.Q = min_secondary;

hotin.P = Pin_primary;
hotout.P = Pin_primary;

hotin.Q = min_primary;
hotout.Q = min_primary;

SEF = A_developed/A_projected;

    A_projected = Lp*Lw;
    Lp = Lv-Dp;
    Lw = Lh+Dp;
    
A_developed = A/N_effective;
Nt = ceil(N_effective) + 2;
Ncp = Nt-1/(2*Np);
b = P-t;
P = Lc/Nt;
Dh = 2*b/SEF;
Ach = b*Lw;

primaryin.state = CP.setProperty(Pin_primary,hotin.h,fluid1,"ph");
primaryin.T = Tin_primary;

splprimaryin.state = CP.setProperty(Pin_primary,primaryin.h,fluid1,"SpecialProperties");

primaryout.state = CP.setProperty(Pin_primary,hotout.h,fluid1,"ph");
primaryout.T = Tout_primary;
splprimaryout.state = CP.setProperty(Pin_primary,primaryout.h,fluid1,"SpecialProperties");

secondaryin.state = CP.setProperty(Pin_secondary,coldin.h,fluid2,"ph");
secondaryin.T = Tin_secondary;

splsecondaryin.state  = CP.setProperty(Pin_secondary,secondaryin.h,fluid2,"SpecialProperties");
secondaryout.state = CP.setProperty(Pin_secondary,Tout_secondary,fluid2,"pT");
secondaryout.h = coldout.h;

splsecondaryout.state = CP.setProperty(Pin_secondary,secondaryout.h,fluid2,"SpecialProperties");


mch_primary = min_primary/Ncp;
G_primary = mch_primary/Ach;

mch_secondary = min_secondary/Ncp;
G_secondary = mch_secondary/Ach;

Re_primary = G_primary*Dh/splprimaryin.viscosity;
Re_secondary = G_secondary*Dh/splsecondaryin.viscosity;

Prandtl_avg = (splprimaryin.Prandtl+splsecondaryin.Prandtl)/2;
Viscosity_avg = (splprimaryin.viscosity+splsecondaryin.viscosity)/2;

f_primary = 0.085*exp(1.52*tan(60))*(Re_primary^(-(0.25-0.06*tan(60))));
Nu_primary = 0.051*exp(0.64*tan(60))*(Re_primary^0.73)*(splprimaryin.Prandtl^0.43)*(splprimaryin.Prandtl/Prandtl_avg)^0.25;

 f_secondary = 0.085*exp(1.52*tan(60))*(Re_secondary^(-(0.25-0.06*tan(60))));
Nu_secondary = 0.051*exp(0.64*tan(60))*(Re_secondary^0.73)*(splsecondaryin.Prandtl^0.43)*(splsecondaryin.Prandtl/Prandtl_avg)^0.25;

Nu_primary = h_primary*Dh/splprimaryin.Conductivity;
Nu_secondary = h_secondary*Dh/splsecondaryin.Conductivity;

(1/U_clean) = (1/h_primary)+(1/h_secondary)+(t/K);

(1/U_fouled) = (1/U_clean)+ 0.0000827;

Qr = min_primary*splprimaryin.CpMass*(abs(Tin_primary-Tout_primary));
Qf = U_fouled*(SEF*Lp*Lw*N_effective)*(((Tin_primary-Tout_secondary)/2)+((Tout_primary-Tin_secondary)/2));

CF = U_fouled/U_clean;
CS = Qf/Qr;

min_secondary*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary) + min_primary*splprimaryin.CpMass*(Tin_primary-Tout_primary) = secondaryin.d*V*splsecondaryin.CpMass*der(Tout_secondary);

min_primary*splprimaryin.CpMass*(Tin_primary-Tout_primary) + min_secondary*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary) = primaryin.d*V*splprimaryin.CpMass*der(Tout_primary);

//min_secondary*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary) - min_primary*splprimaryin.CpMass*(Tin_primary-Tout_primary) = secondaryin.d*V*splsecondaryin.CpMass*der(Tout_secondary);

//min_primary*splprimaryin.CpMass*(Tin_primary-Tout_primary) - min_secondary*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary) = primaryin.d*V*splprimaryin.CpMass*der(Tout_primary);

annotation(
    Icon(graphics = {Rectangle(extent = {{-100, 100}, {100, -100}})}));
    
    end PlateHeatExchanger;
