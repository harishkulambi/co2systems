within HeatExchangers;

model PlateHeatExchanger_Example
  extends Modelica.Icons.Example;
  HeatExchangers.PlateHeatExchanger plateHeatExchanger1(Tout_primary(start = 373.15), Tout_secondary(start = 273.15))  annotation(
    Placement(visible = true, transformation(origin = {6, -8}, extent = {{-34, -34}, {34, 34}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 7.6e+06, Q0 = 2.16, T0 = 423.15)  annotation(
    Placement(visible = true, transformation(origin = {86, 34}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT2(P0 = 400000, Q0 = 2, T0 = 298.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-70, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1 annotation(
    Placement(visible = true, transformation(origin = {-70, -50}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {88, -62}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(source_PQT2.op, plateHeatExchanger1.coldin) annotation(
    Line(points = {{-59, 30}, {-32, 30}, {-32, 16}}));
  connect(plateHeatExchanger1.coldout, sink2.ip) annotation(
    Line(points = {{44, -32}, {78, -32}, {78, -62}, {78, -62}}));
  connect(sink1.ip, plateHeatExchanger1.hotout) annotation(
    Line(points = {{-60, -50}, {-32, -50}, {-32, -32}, {-32, -32}}));
  connect(plateHeatExchanger1.hotin, source_PQT1.op) annotation(
    Line(points = {{44, 16}, {76, 16}, {76, 34}, {76, 34}}));
end PlateHeatExchanger_Example;
