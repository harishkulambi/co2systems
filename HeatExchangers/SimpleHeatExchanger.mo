within HeatExchangers;

model SimpleHeatExchanger
  extends Modelica.Icons.Example;
  
  import SI = Modelica.SIunits;
  
  parameter Integer n = 10;
  parameter SI.Length L = 1;
  parameter SI.Radius R = 0.1;
  parameter SI.Density rho = 2;
  parameter SI.CoefficientOfHeatTransfer h = 2;
  parameter SI.ThermalConductivity K = 10;
  parameter SI.SpecificHeatCapacityAtConstantPressure C = 10;
  parameter SI.Temperature Tamb = 30+273.15;
  
  SI.Area A;
  SI.Volume V;
  SI.Temperature T1[n];

  initial equation 
    T1 = linspace(0,500,n);

    
equation
  A = (Modelica.Constants.pi)*R^2;
  V = A*L/n;
  
  rho*V*C*der(T1[1]) = (-h*(T1[1]-Tamb)) - (-K*A*(T1[1]-T1[2])/(L/n));
  
  
  
  for i in 2:(n-1) loop 
    rho*V*C*der(T1[i]) = (-K*A*(T1[i]-T1[i-1])/(L/n)) - (K*A*(T1[i]-T1[i+1])/(L/n));
    
    
  end for;

rho*V*C*der(T1[n]) = -h*(T1[n] - Tamb) - (K*A*(T1[n]-T1[n-1])/(L/n));

//  for i in 1:n loop
//    rho*V*C*der(T1[i]) = (K*A*(T1[i] - T1[i-1])/(L/n)) - (K*A*(T1[i] - T1[i+1])/(L/n));
//  end for;

   
end SimpleHeatExchanger;
