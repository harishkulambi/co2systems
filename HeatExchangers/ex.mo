within HeatExchangers;

model ex

parameter Real m = 2,Cp = 4180,Tin = 25+273.15,d = 1000,V = 1,L = 2;
Real Tout;
equation

m*Cp*(Tin-Tout) = d*V*Cp*(Tin-Tout)/L;
end ex;
