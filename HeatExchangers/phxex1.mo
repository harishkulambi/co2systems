within HeatExchangers;

model phxex1

parameter Integer n = 10;

import SI = Modelica.SIunits;

parameter SI.Volume V = 1/n;

parameter SI.MassFlowRate min_primary = 2;
parameter SI.MassFlowRate min_secondary = 2;

parameter SI.CoefficientOfHeatTransfer Cp_primary = 1166.5;

parameter SI.CoefficientOfHeatTransfer Cp_secondary = 4180.5;

parameter SI.Temperature Tin_primary = 150+273.15;
parameter SI.Temperature Tin_secondary = 25+273.15;

SI.Temperature Tout_primary[n];
SI.Temperature Tout_secondary[n];

SI.Temperature Tin_primary1[n];
SI.Temperature Tin_secondary1[n];

//initial equation 
//  Tout_primary[n] = 170+273.15;
//  Tout_secondary[n] = 20+273.15;
  
equation

min_primary*Cp_primary*(Tin_primary-Tout_primary[1]) + min_secondary*Cp_secondary*(Tin_secondary-Tout_secondary[1]) = 106.67*V*Cp_primary*der(Tout_primary[1]);
min_secondary*Cp_secondary*(Tin_secondary-Tout_secondary[1]) + min_primary*Cp_primary*(Tin_primary-Tout_primary[1]) = 1000*V*Cp_secondary*der(Tout_secondary[1]);

Tin_primary1[1] = Tout_primary[1];
Tin_secondary1[1] = Tout_secondary[1];

for i in 2:n loop

    Tin_primary1[i] = Tout_primary[i-1];
    Tin_secondary1[i] = Tout_secondary[i-1];
    
    
//    min_primary*Cp_primary*(Tout_primary[i-1]-Tout_primary[i]) + min_secondary*Cp_secondary*(Tout_secondary[i-1]-Tout_secondary[i]) = 106.67*V*Cp_primary*der(Tout_primary[i]);
    
//    min_secondary*Cp_secondary*(Tout_secondary[i-1]-Tout_secondary[i]) + min_primary*Cp_primary*(Tout_primary[i-1]-Tout_primary[i]) = 1000*V*Cp_secondary*der(Tout_secondary[i]);
  
    min_primary*Cp_primary*(Tin_primary1[i]-Tout_primary[i]) = 106.67*V*Cp_primary*der(Tout_primary[i]);
    min_secondary*Cp_secondary*(Tin_secondary1[i]-Tout_secondary[i]) = 1000*V*Cp_secondary*der(Tout_secondary[i]);  
    
   end for;
   
   

//min_primary*Cp_primary*(Tout_primary[n-1]-Tout_primary[n]) + min_secondary*Cp_secondary*(Tout_secondary[n-1]-Tout_secondary[n]) = 106.67*V*Cp_primary*der(Tout_primary[n]);
//min_secondary*Cp_secondary*(Tout_secondary[n-1]-Tout_secondary[n]) + min_primary*Cp_primary*(Tout_primary[n-1]-Tout_primary[n]) = 1000*V*Cp_secondary*der(Tout_secondary[n]);

//Tin_primary1[n] = Tout_primary[n-1];
//Tin_secondary1[n] = Tout_secondary[n-1];


    
end phxex1;
