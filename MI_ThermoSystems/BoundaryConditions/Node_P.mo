within MI_ThermoSystems.BoundaryConditions;

model Node_P
  extends Modelica.Icons.MaterialPropertiesPackage;
    import SI = Modelica.SIunits;
    
  parameter SI.AbsolutePressure P0 = 10e5 "Fluid Pressure (active if IPressure connector is not connected)";
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected 
  Modelica.Blocks.Interfaces.RealInput InP annotation(
    Placement(visible = true, transformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));


equation
  
  /*Pressure*/
  
    if (cardinality(InP) == 0) then 
      InP = P0;
    end if;
   
   ip.P = InP;
//   ip.P = op.P;
//   ip.h = op.h;
ip.Q = op.Q;
   
end Node_P;
