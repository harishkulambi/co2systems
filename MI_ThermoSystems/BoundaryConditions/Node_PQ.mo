within MI_ThermoSystems.BoundaryConditions;

model Node_PQ
  extends Modelica.Icons.MaterialPropertiesPackage;
  
    import SI = Modelica.SIunits;
    
    parameter SI.AbsolutePressure P0 = 10e5 "Fluid Pressure (active if IPressure connector is not connected)";
  //    parameter SI.MassFlowRate     Q0 = 100  "Fluid Mass Flow (active if IMassFlow connector is not connected)";
    parameter SI.Temperature T0 = 75+273.15;
    
    parameter String fluid = "carbondioxide";
    CoolPropLinker.Interfaces.Functions CP;
    CoolPropLinker.Interfaces.BaseProperties prop;
    
//  parameter SI.Pressure Pout = 76e5;
  
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
//  Modelica.Blocks.Interfaces.RealOutput y annotation(
//    Placement(visible = true, transformation(origin = {106, 68}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

protected
  
  Modelica.Blocks.Interfaces.RealInput InP annotation(
    Placement(visible = true, transformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealInput InT annotation(
    Placement(visible = true, transformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90), iconTransformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90)));

equation

//  y = P0;
  /*Pressure*/
    
    if (cardinality(InP) == 0) then
      InP = P0;
    end if;
    
    op.P = InP;
  
//    ip.P = op.P;

/*Mass Flow Rate*/
    if (cardinality(InT) == 0) then 
      InT = T0;
    end if;
    
    prop.state = CP.setProperty(InP,InT,fluid,"pT");
    prop.h = op.h;

    ip.h = op.h;

          
end Node_PQ;
