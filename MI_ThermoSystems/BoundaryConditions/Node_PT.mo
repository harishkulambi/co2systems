within MI_ThermoSystems.BoundaryConditions;

model Node_PT
  extends Modelica.Icons.MaterialPropertiesPackage;
  
    import SI = Modelica.SIunits;
    
     parameter SI.AbsolutePressure P0 = 100e5  "Fluid Pressure (active if IPressure connector is not connected)";
//    SI.MassFlowRate     Q0   "Fluid Mass Flow (active if IMassFlow connector is not connected)";
    parameter SI.Temperature      T0 = 70+273.15;
  
   parameter String fluid = "carbondioxide";
      //Fluid naming convention based on CoolProp Python wrapper
    CoolPropLinker.Interfaces.Functions CP;
      // Function to calculate properties
    CoolPropLinker.Interfaces.BaseProperties prop;
  
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput InT annotation(
    Placement(visible = true, transformation(origin = {-120, 52}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 52}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
protected
  
  Modelica.Blocks.Interfaces.RealInput InP annotation(
    Placement(visible = true, transformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
//  Modelica.Blocks.Interfaces.RealInput InQ annotation(
//    Placement(visible = true, transformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90), iconTransformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90)));

equation

  /*Pressure*/
    
    if (cardinality(InP) == 0) then
      InP = P0;
    end if;
    
//    ip.P = InP;
    ip.P = op.P;

    if (cardinality(InT) == 0) then   
        InT = T0;
    end if;
      
      prop.state = CP.setProperty(InP,InT,fluid,"pT");
      prop.h = ip.h;  
       
      op.h = ip.h;
end Node_PT;
