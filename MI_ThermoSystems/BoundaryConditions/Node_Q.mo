within MI_ThermoSystems.BoundaryConditions;

model Node_Q
  
  extends Modelica.Icons.MaterialPropertiesPackage;
    
    import SI = Modelica.SIunits;
    
    parameter SI.MassFlowRate Q0 = 100 "Fluid Mass Flow (active if IMassFlow connector is not connected)";
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-104, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected 
  Modelica.Blocks.Interfaces.RealInput InQ annotation(
    Placement(visible = true, transformation(origin = {-120, 80}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 80}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));

equation
  
  /*Mass Flow Rate*/
  
    if (cardinality(InQ) == 0) then
      InQ = Q0;
    end if;
    
    ip.Q = InQ;
    ip.Q = op.Q;
    ip.h = op.h;  
   
end Node_Q;
