within MI_ThermoSystems.BoundaryConditions;

model Node_T
  extends Modelica.Icons.MaterialPropertiesPackage;
  
    import SI = Modelica.SIunits;
    
    parameter SI.Temperature T0 = 298 "Fluid Temperature (active if IEnthalpy connector is not connected)";
    
    parameter String fluid = "carbondioxide";
      // Fluid convention naming based on CoolProp Python wrapper
      
    CoolPropLinker.Interfaces.Functions CP;
      //Function to calculate properties
      
    CoolPropLinker.Interfaces.BaseProperties prop;
      //Properties at the port
      
    MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-116, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected 
    Modelica.Blocks.Interfaces.RealInput InT annotation(
    Placement(visible = true, transformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));

equation
    
    /*Temperature*/
    
    if (cardinality(InT) == 0) then
      InT = T0;
    end if;
//    ip.P = op.P;
//ip.Q = op.Q;
ip.h = op.h;
    prop.state = CP.setProperty(ip.P,InT,fluid,"pT");
    prop.h = ip.h;
end Node_T;
