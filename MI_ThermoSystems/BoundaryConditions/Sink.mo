within MI_ThermoSystems.BoundaryConditions;

model Sink
  
  extends Modelica.Icons.SourcesPackage;
  
    import SI = Modelica.SIunits;
    
    SI.Temperature T;
    
    parameter String fluid = "carbondioxide";
     SI.Pressure psink ;

// Fluid Convention naming based on Cool Prop Python wrapper
    CoolPropLinker.Interfaces.Functions CP;
      // Function to calculate properties
    CoolPropLinker.Interfaces.BaseProperties prop;
    Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  //Properties at the port
equation
    ip.P = psink;
    prop.state = CP.setProperty(ip.P,ip.h,fluid,"ph");
    prop.T = T;
    
end Sink;
