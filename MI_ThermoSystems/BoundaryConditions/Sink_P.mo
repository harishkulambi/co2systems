within MI_ThermoSystems.BoundaryConditions;

model Sink_P
  
  extends Modelica.Icons.SourcesPackage;
  
    import SI = Modelica.SIunits;
    
    parameter SI.AbsolutePressure P0 = 10e5 "Fluid Pressure (active if IPressure connector is not connected)";
    
//    parameter SI.Temperature T = 30+273.15;
SI.Temperature T;
    
//    SI.SpecificEnthalpy h;
    parameter String fluid = "carbondioxide";
      // Fluid convention naming based on CoolProp python wrapper
    
    CoolPropLinker.Interfaces.Functions CP;
      // Functions to calculate properties
    
    CoolPropLinker.Interfaces.BaseProperties prop;
      // Properties at the port
    
    
    
    MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected 
  Modelica.Blocks.Interfaces.RealInput InP annotation(
    Placement(visible = true, transformation(origin = {120, 60}, extent = {{20, -20}, {-20, 20}}, rotation = 0), iconTransformation(origin = {120, 60}, extent = {{20, -20}, {-20, 20}}, rotation = 0)));
equation

  if (cardinality(InP) == 0) then 
    InP = P0;
  end if;
  
  ip.P = InP;
  
  prop.state = CP.setProperty(InP,ip.h,fluid,"ph");
  prop.T = T;

//prop.state = CP.setProperty(InP,T,fluid,"pT");
//  prop.h = h;
  
//  ip.h = h;
end Sink_P;
