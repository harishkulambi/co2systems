within MI_ThermoSystems.BoundaryConditions;

model Source_PH
  extends Modelica.Icons.SourcesPackage;
  
    import SI = Modelica.SIunits;
    
    parameter SI.AbsolutePressure P0 = 303975 "Fluid Pressure (active if IPressure connector is not connected)";
    parameter SI.Enthalpy         H0 = 400e3 "Fluid Enthalpy (active if IEnthalpy connector is not connected)";
    
    parameter String fluid = "carbondioxide";
    

    MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {108, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected  
  Modelica.Blocks.Interfaces.RealInput InP annotation(
    Placement(visible = true, transformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput InH annotation(
    Placement(visible = true, transformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));

equation
    
    /*Pressure*/
    
    if (cardinality(InP) == 0) then
      InP = P0;
    end if;
    
    op.P = InP;
    
    /*Enthalpy*/
      
    if (cardinality(InH) == 0) then
      InH = H0;
    end if;
    
    
    op.h = InH; 
      
annotation(
    Diagram(graphics = {Text(origin = {-135, 61}, extent = {{-7, 7}, {7, -7}}, textString = "P"), Text(origin = {-131, -60}, extent = {{-5, 6}, {5, -6}}, textString = "H")}, coordinateSystem(initialScale = 0.1)));

end Source_PH;
