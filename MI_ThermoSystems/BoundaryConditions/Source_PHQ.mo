within MI_ThermoSystems.BoundaryConditions;

model Source_PHQ

  extends Modelica.Icons.SourcesPackage;  
    import SI = Modelica.SIunits;
    
    parameter SI.AbsolutePressure P0 = 303975 "Fluid Pressure (active if IPressure connector is not connected)";
    parameter SI.SpecificEnthalpy H0 = 400e3 "Fluid Enthalpy  (active if IPressure connector is not connected)";
    parameter SI.MassFlowRate     Q0 = 100 "Fluid Mass Flow Rate  (active if IMassFlow connector is not connected)";
    
    parameter String fluid = "carbondioxide";
      //Fluid naming convention based on CoolProp Python wrapper
    
    
    MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {108, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected  
  Modelica.Blocks.Interfaces.RealInput InP annotation(
    Placement(visible = true, transformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90), iconTransformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90)));
  Modelica.Blocks.Interfaces.RealInput InH annotation(
    Placement(visible = true, transformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput InQ annotation(
    Placement(visible = true, transformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));

equation
  
  /*Pressure*/
    if (cardinality(InP) == 0) then 
      InP = P0;
    end if;
    
    op.P = InP;
    
  /*Enthalpy*/
    if (cardinality(InH) == 0) then 
      InH = H0;
    end if;
    
    op.h = InH;
    
  /*Mass Flow Rate*/
    if (cardinality(InQ) == 0) then 
      InQ = Q0;
    end if;
    
    op.Q = InQ;
annotation(
    Diagram(graphics = {Text(origin = {2, 135}, extent = {{-14, 9}, {14, -9}}, textString = "Q"), Text(origin = {-136, 2}, extent = {{-20, 12}, {20, -12}}, textString = "H"), Text(origin = {2, -136}, extent = {{-20, 12}, {20, -12}}, textString = "P")}));
    end Source_PHQ;
