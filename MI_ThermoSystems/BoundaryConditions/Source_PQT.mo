within MI_ThermoSystems.BoundaryConditions;

model Source_PQT

  extends Modelica.Icons.SourcesPackage;
  
    import SI = Modelica.SIunits;
    
    parameter SI.AbsolutePressure P0 = 303975 "Fluid Pressure (active if IPressure connector is not connected)";
    
    parameter SI.MassFlowRate Q0 = 100 "Fluid Mass Flow Rate if IMassFlow connector is not connected)";
    
    parameter SI.Temperature T0 = 298 "Fluid Enthalpy (active if IEnthalpy connector is not connected)";
    
    parameter String fluid = "carbondioxide";
      //Fluid naming convention based on CoolProp Python wrapper
    CoolPropLinker.Interfaces.Functions CP;
      // Function to calculate properties
    CoolPropLinker.Interfaces.BaseProperties prop;

// Properties at the port

    Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected   
    Modelica.Blocks.Interfaces.RealInput InP annotation(
      Placement(visible = true, transformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90), iconTransformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90)));
    Modelica.Blocks.Interfaces.RealInput InT annotation(
      Placement(visible = true, transformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
    Modelica.Blocks.Interfaces.RealInput InQ annotation(
    Placement(visible = true, transformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));

equation

  /*Pressure*/
    
    if (cardinality(InP) == 0) then
      InP = P0;
    end if;
    
    op.P = InP;
    
  /*Temperature*/
    
    if (cardinality(InT) == 0) then
      InT = T0;
    end if;
    
    prop.state = CP.setProperty(InP,InT,fluid,"pT");
    op.h = prop.h;
    
  /*MassFlowRate*/
    
    if (cardinality(InQ) == 0) then
      InQ = Q0;
    end if;
    
    op.Q = InQ;
    
annotation(
    Icon(graphics = {Text(origin = {-132, 5}, extent = {{-4, 1}, {4, -1}}, textString = "text")}),
    Diagram(graphics = {Text(origin = {-2, 144}, extent = {{-18, 14}, {18, -14}}, textString = "Q"), Text(origin = {1, -138}, extent = {{-11, 14}, {11, -14}}, textString = "P"), Text(origin = {-136, 1}, extent = {{-14, 15}, {14, -15}}, textString = "T")}, coordinateSystem(initialScale = 0.1)));
    
    end Source_PQT;
