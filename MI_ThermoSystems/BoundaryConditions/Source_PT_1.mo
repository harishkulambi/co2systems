within MI_ThermoSystems.BoundaryConditions;

model Source_PT_1
  extends Modelica.Icons.SourcesPackage;
  
  
  import SI = Modelica.SIunits;
  
  parameter SI.AbsolutePressure P0 = 303975 "Fluid Pressure (active if IPressure connector is not connected)";
  parameter SI.Temperature T0 = 298 "Fluid Enthalpy (active if IEnthalpy connector is not connected)";
  SI.SpecificEnthalpy h;
  
  parameter String fluid = "carbondioxide"; //
  CoolPropLinker.Interfaces.Functions CP;
  //Functions to calculate properties
  CoolPropLinker.Interfaces.BaseProperties prop;
  //Properties at the port
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {108, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput Pout annotation(
    Placement(visible = true, transformation(origin = {110, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected
  Modelica.Blocks.Interfaces.RealInput InP annotation(
    Placement(visible = true, transformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput InT annotation(
    Placement(visible = true, transformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
equation
/*Pressure*/
  if (cardinality(InP) == 0) then
    InP = P0;
  end if;
 
/*Enthalpy*/
  if (cardinality(InT) == 0) then
    InT = T0;
  end if;
  prop.state = CP.setProperty(InP, InT, fluid, "pT");
  prop.h = h;
  
   op.P = P0;
  op.h = h;
  
  Pout = InP;
//  op.Q = Q;
  annotation(
    Diagram(graphics = {Text(origin = {-135, 61}, extent = {{-7, 7}, {7, -7}}, textString = "P"), Text(origin = {-131, -60}, extent = {{-5, 6}, {5, -6}}, textString = "T")}));
end Source_PT_1;
