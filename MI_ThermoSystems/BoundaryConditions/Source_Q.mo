within MI_ThermoSystems.BoundaryConditions;

model Source_Q
  extends Modelica.Icons.SourcesPackage;
    
    import SI = Modelica.SIunits;
     
    parameter String fluid = "carbodioxide";
    // Fluid convention naming based on CoolProp Python wrapper
  parameter SI.MassFlowRate Q0 = 100;    
//    CoolPropLinker.Interfaces.Functions CP;
//    // Functions to calculate properties
    
//    CoolPropLinker.Interfaces.BaseProperties prop;
//    // Properties at the port
  
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected 
//  Modelica.Blocks.Interfaces.RealInput Quality annotation(
//    Placement(visible = true, transformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput InQ annotation(
    Placement(visible = true, transformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));

equation
  
  /*Temperature*/
  
  if (cardinality(InQ) == 0) then
    InQ = Q0;
  
  end if;
  
  op.Q = InQ;
  
    
annotation(
    Diagram(graphics = {Text(origin = {-134, -57}, extent = {{-14, 9}, {14, -9}}, textString = "T"), Text(origin = {-136, 58}, extent = {{-24, 14}, {14, -6}}, textString = "Quality")}, coordinateSystem(initialScale = 0.1)));


end Source_Q;
