within MI_ThermoSystems.BoundaryConditions;

model Source_T
  extends Modelica.Icons.SourcesPackage;
    
    import SI = Modelica.SIunits;
  
    parameter SI.Temperature  T0 = 298 "Fluid Enthalpy (active if IEnthalpy connector is not connected)";
    parameter Real      Quality0 = 0.3 "Fluid Quality (active if IQuality connector is not connected)";
    parameter Real massflow = 2;
    
    parameter String fluid = "carbodioxide";
    // Fluid convention naming based on CoolProp Python wrapper
    
    CoolPropLinker.Interfaces.Functions CP;
    // Functions to calculate properties
    
    CoolPropLinker.Interfaces.BaseProperties prop;
    // Properties at the port
  
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected 
  Modelica.Blocks.Interfaces.RealInput Quality annotation(
    Placement(visible = true, transformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput InT annotation(
    Placement(visible = true, transformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, -60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));

equation
  
  /*Temperature*/
  
  if (cardinality(InT) == 0) then
    InT = T0;
    Quality = Quality0;
  end if;
  
  prop.state = CP.setProperty(InT,Quality,fluid,"TQ");
  prop.h = op.h;
  prop.p = op.P;
 
  op.Q = massflow;   
annotation(
    Diagram(graphics = {Text(origin = {-134, -57}, extent = {{-14, 9}, {14, -9}}, textString = "T"), Text(origin = {-136, 58}, extent = {{-24, 14}, {14, -6}}, textString = "Quality")}, coordinateSystem(initialScale = 0.1)));

end Source_T;
