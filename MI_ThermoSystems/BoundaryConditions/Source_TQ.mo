within MI_ThermoSystems.BoundaryConditions;

model Source_TQ "Source for two phase fluid"
  extends Modelica.Icons.SourcesPackage;
    
    import SI = Modelica.SIunits;
  
    parameter SI.Temperature  T0 = 298 "Fluid Enthalpy (active if IEnthalpy connector is not connected)";
    parameter SI.MassFlowRate Q0 = 100 "Fluid Mass Flow (active if IMassFlow connector is not connected)";
    parameter Real      Quality0 = 0.3 "Fluid Quality (active if IQuality connector is not connected)";
    
    parameter String fluid = "carbondioxide";
    // Fluid convention naming based on CoolProp Python wrapper
    
    CoolPropLinker.Interfaces.Functions CP;
    // Functions to calculate properties
    
    CoolPropLinker.Interfaces.BaseProperties prop;
    // Properties at the port
  
    SI.Pressure P;
    SI.SpecificEnthalpy H;
    
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
protected 
  Modelica.Blocks.Interfaces.RealInput Quality annotation(
    Placement(visible = true, transformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput InQ annotation(
    Placement(visible = true, transformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {0, 120}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  Modelica.Blocks.Interfaces.RealInput InT annotation(
    Placement(visible = true, transformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90), iconTransformation(origin = {0, -120}, extent = {{-20, -20}, {20, 20}}, rotation = 90)));

equation
  
  /*Temperature*/
  
  if (cardinality(InT) == 0) then
    InT = T0;
//    Quality = Quality0;
  end if;
  if (cardinality(InQ) == 0) then 
    InQ = Q0;
  end if;
  
  if (cardinality(Quality) == 0) then 
    Quality = Quality0;
  end if;
  
  prop.state = CP.setProperty(InT,Quality,fluid,"TQ");
  prop.h = H;
  prop.p = P;
  /*Mass Flow Rate*/
  
//  if (cardinality(InQ) == 0) then 
//    InQ = Q0;
//  end if;
  
  op.Q = InQ;
  op.P = P;
  op.h = H;  
annotation(
    Diagram(graphics = {Text(origin = {2, -143}, extent = {{-14, 9}, {14, -9}}, textString = "T"), Text(origin = {-142, -4}, extent = {{-24, 14}, {14, -6}}, textString = "Quality"), Text(origin = {-2, 142}, extent = {{-12, 10}, {12, -10}}, textString = "Q")}));
    end Source_TQ;
