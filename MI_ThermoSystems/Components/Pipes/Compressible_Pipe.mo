within MI_ThermoSystems.Components.Pipes;

model Compressible_Pipe


  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
    //Fluid convention naming based on CoolProp Python wrapper
  
    CoolPropLinker.Interfaces.Functions CP;
      //Function to calculate properties
    CoolPropLinker.Interfaces.BaseProperties pipein;
      //Pipe inlet properties 
    CoolPropLinker.Interfaces.BaseProperties pipeout;
      //Pipe outlet properties 
    CoolPropLinker.Interfaces.SpecialProperties spl_pipein;
      // Pipe inlet special properties
    
  SI.Pressure Pin "Pipe inlet pressure";
  SI.Pressure P "Mean toatl pressure in the element";
  SI.Pressure delP "Pressure drop in the pipe";
  
  SI.Temperature Tin "Pipe inlet temperature";
  SI.Temperature T "Mean temperature at the inlet and outlet";
  
  SI.SpecificEnthalpy hin "Specific enthalpy at the inlet";
  SI.SpecificEnthalpy hout "specific enthalpy at the outlet";
  
  SI.Density rho "Density of the fluid";
  SI.Velocity V "Velocity of the pipe in fluid";
  SI.MassFlowRate massflowrate "Mass flow rate of the fluid";
  SI.Area A "Cross sectional area of the pipe";
  SI.DynamicViscosity Mu "dynamic Viscosity at the inlet";
  
  Real Mach_no "Ratio of flow velocity to local speed of sound";
  Real Reynolds_no "Reynolds number of the fluid";
  Real f "Friction factor";
  
  parameter Real e = 0.045 "Surface roughness of pipe";
  parameter Real gamma = 1.3 "Ratio of specific heats";
  parameter Real K = 1 "Inlet and exit losses";
  parameter SI.Temperature delT = 0 "Total temperature difference";
  parameter SI.Height delZ = 0 "Height difference";
  parameter SI.Diameter D = 22e-3 "Inside diameter of pipe";
  parameter SI.Length L = 1 "Length of the pipe";


protected
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation

  ip.P = Pin;
  ip.h = hin;
  ip.Q = massflowrate;
  
  V = massflowrate/(rho*A);
  A = (Modelica.Constants.pi/4)*D^2;
  
  pipein.state = CP.setProperty(Pin,hin,fluid,"ph");
  pipein.d = rho;
  pipein.T = Tin;
  
  spl_pipein.state = CP.setProperty(Pin,hin,fluid,"SpecialProperties");
  spl_pipein.viscosity = Mu;
  
  Reynolds_no = (rho*V*D)/Mu;
  f = if Reynolds_no < 2300 then 64/Reynolds_no else 0.25/(log((e/(3.7*D))+(5.74/Reynolds_no^0.9)))^2;
  Mach_no = V/sqrt(gamma*Modelica.Constants.R*T);
  
  T = (Tin + delT + Tin)/2;
  P = (Pin + delP + Pin)/2;
  
  delP = ((f*L/D)+(delT/T))*(P*gamma*Mach_no^2)/2 + (P/Pin)*((rho*Modelica.Constants.g_n*delZ)+(0.5*K*rho*V^2) + (0.5*K*rho*V^2)) + (P*rho*L/Pin)*der(V);
  
  pipeout.state = CP.setProperty(Pin-delP,Tin,fluid,"pT");
  pipeout.h = hout;
  
  op.P = Pin - delP;
  op.h = hout;
  op.Q = massflowrate;
  
annotation(
    Icon(graphics = {Rectangle(fillColor = {255, 255, 127}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, 40}, {100, -40}})}));
    end Compressible_Pipe;
