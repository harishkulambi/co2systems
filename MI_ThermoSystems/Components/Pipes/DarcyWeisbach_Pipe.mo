within MI_ThermoSystems.Components.Pipes;

model DarcyWeisbach_Pipe
    
    import SI = Modelica.SIunits;
    
    parameter String fluid = "carbondioxide";
      //Fluid convention naming based on CoolProp python wrapper
      
    CoolPropLinker.Interfaces.Functions CP;
      //Functions to calcualte properties
    CoolPropLinker.Interfaces.BaseProperties pipein;
      //Pipe inlet Properties
    CoolPropLinker.Interfaces.BaseProperties pipeout;
      //Pipe outlet properties
      
    parameter SI.Length L = 1 "Length of the pipe";
    parameter SI.Diameter D = 0.01 "Diameter of the pipe";
    parameter Real f = 0.05 "Frictional coefficient of the pipe";
    
    SI.Area A "Area of the pipe";
    SI.Velocity Vel "Velocity of the fluid";
    SI.Volume V "Volume of the pipe";
    SI.VolumeFlowRate Vf "Volume flow rate of the pipe";
    SI.Pressure delP "Pressure drop through the pipe";
    SI.Density rho "Density of the fluid";
    
    SI.Pressure Pin "Inlet pressure";
    SI.Pressure Pout "Outlet pressure";
    
    SI.Temperature Tin "Inlet temperature";
    SI.Temperature Tout "Outlet temperature";
protected
    MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation
  
    ip.P = Pin;
    ip.Q = op.Q;
    ip.h = op.h;
    
    pipein.state = CP.setProperty(Pin,ip.h,fluid,"ph");
    pipein.d = rho;
    pipein.T = Tin;
    A = (Modelica.Constants.pi/4)*D^2;
    ip.Q = rho*A*Vel;
    V = A*L;
    Vf = A*Vel;
    delP = (f*rho*L*Vel^2)/(2*D);
    Pout = Pin - delP;
  
    pipeout.state = CP.setProperty(Pout,op.h,fluid,"ph");
    pipeout.T = Tout;
        
    op.P = Pout;
annotation(
    Diagram(coordinateSystem(initialScale = 0.1)),
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, 60}, {100, -60}})}));
    end DarcyWeisbach_Pipe;
