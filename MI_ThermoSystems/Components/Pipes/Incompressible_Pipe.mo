within MI_ThermoSystems.Components.Pipes;

model Incompressible_Pipe

  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
    //Fluid convention naming based on coolprop python wrapper
    
  CoolPropLinker.Interfaces.Functions CP;
    //Function to calculate properties
  CoolPropLinker.Interfaces.BaseProperties pipein;
    //Properties at the pipe inlet 
  CoolPropLinker.Interfaces.BaseProperties pipeout;
    // Properties at the pipe outlet
  CoolPropLinker.Interfaces.SpecialProperties splin;
    //Special properties at the inlet
    

  parameter SI.Diameter D = 22e-2 "Diameter of the pipe";
  parameter SI.Length L = 1 "Length of the pipe";
  parameter Real K = 1 "Loss Constant";
  parameter Real e = 0.045 "Mean inside wall roughness";
  parameter SI.Height delZ = 0 "Altitute difference";
  
  SI.Pressure Pin "Pipe inlet pressure";
  SI.Pressure Pout "Pipe outlet pressure";
  SI.Pressure delP "Pipe pressure drop";
  
  SI.Temperature Tin "Pipe inlet temperature";
  SI.Temperature Tout "Pipe outlet temperature";
  
  SI.SpecificEnthalpy hin "Pipe inlet enthalpy";
  SI.SpecificEnthalpy hout "Pipe outlet enthalpy";
  
  SI.Density rho "Density of the fluid";
  SI.MassFlowRate massflow "Mass flow rate of the fluid";
  SI.Area A "Area of the pipe";
  SI.Velocity Vel"Velocity of the fluid in pipe";
  SI.DynamicViscosity Mu "Viscosity of the fluid";
  
  Real f "Frictional loss coefficient";
  Real Reynolds_no "Reynolds number of fluid";
  
protected
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));


equation

  ip.P = Pin;
  ip.Q = massflow;
  ip.h = hin;
  
  pipein.state = CP.setProperty(Pin,Tin,fluid,"pT");
  pipein.h = hin;
  pipein.d = rho;
  
  splin.state = CP.setProperty(Pin,hin,fluid,"ph");
  splin.viscosity = Mu;
  
  A = (Modelica.Constants.pi/4)*D^2;
  massflow = rho*A*Vel;
  Reynolds_no = rho*Vel*D/Mu;
  
  f = if Reynolds_no <2300 then 64/Reynolds_no else 0.25/(log((e/3.5*D)+(5.74/Reynolds_no^0.9)))^2;
  
  delP = (f*L/D)*(rho*Vel^2) + (rho*Modelica.Constants.g_n*delZ) + (0.5*K*rho*Vel^2)+ (rho*L*der(Vel)) ;
  
  Pout = Pin - delP;
  
  hin = hout;
  
  pipeout.state = CP.setProperty(Pout,hout,fluid,"ph");
  pipeout.T = Tout;
  
  op.P = Pout;
  op.Q = massflow;
  op.h = hout;
  
annotation(
    Icon(graphics = {Rectangle(fillColor = {170, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, 60}, {100, -60}})}));
    end Incompressible_Pipe;
