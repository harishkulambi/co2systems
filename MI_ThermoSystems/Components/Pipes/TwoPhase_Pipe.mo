within MI_ThermoSystems.Components.Pipes;

model TwoPhase_Pipe

  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
    // Fluid naming convention based on coolprop python wrapper
  
  CoolPropLinker.Interfaces.Functions CP;
    // Function to calculate properties
  CoolPropLinker.Interfaces.BaseProperties pipein;
    // Pipe inlet properties
  CoolPropLinker.Interfaces.BaseProperties pipeout;
    // Pipe outlet properties
  
  CoolPropLinker.Interfaces.BaseProperties liquid;
    // Liquid properties
  CoolPropLinker.Interfaces.BaseProperties vapor;
    // Vapor properties
  CoolPropLinker.Interfaces.SpecialProperties splin;
    //Pipe inlet special properties
      
protected  
    MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
public
  parameter SI.Length L = 1 "Length of the pipe";
  parameter SI.Diameter D = 22e-3 "Diameter of the pipe";
  parameter Real e = 0.045 "Surface Roughness";
  parameter SI.Height delZ = 0 "Height difference between inlet and outle";
  
  SI.Pressure Pin "Pipe inlet Pressure";
  SI.Pressure delP "Pressure drop across the pipe";
  SI.Pressure Pout "Pipe outlet pressure";
  
  SI.Temperature Tin "Pipe inlet Temperature";
  SI.Temperature Tout "Pipe outlet Temperature";
  
  SI.MassFlowRate massflow "Pipe inlet mass flow rate";
  SI.MassFlowRate m_liquid "Liquid mass flow rate";
  SI.MassFlowRate m_vapor "Vapor mass flow rate";
  
  SI.Density rho "Pipe inlet density";
  SI.Density rho_liquid "liquid density";
  SI.Density rho_vapor "vapor density";
  
  SI.SpecificEnthalpy hin "Pipe inlet Enthalpy";
  SI.SpecificEnthalpy hout "Pipe outlet Enthalpy";
   
  SI.DynamicViscosity Mu "Fluid dynamic viscosity";
  
  SI.Velocity Vel "Velocity of the fluid";
  SI.Area A "Area of the pipe";
  
  Real Quality "Vapor fraction of fluid";
  Real phi "Lockheart-Martinelli multiplier";
  Real Reynolds_no "Reynolds number of fluid";
  Real f "Frictional factor";
  
  
equation
  
  ip.P = Pin;
  ip.h = hin;
  ip.Q = massflow;
  
  pipein.state = CP.setProperty(Pin,hin,fluid,"ph");
  pipein.T = Tin;
  pipein.d = rho;
  pipein.Q = Quality;
  
  m_liquid = massflow*(1-Quality);
  m_vapor = massflow*Quality;
  
  liquid.state = CP.setProperty(Pin,0,fluid,"pQ");
  liquid.d = rho_liquid;
  
  vapor.state = CP.setProperty(Pin,1,fluid,"pQ");
  vapor.d = rho_vapor;
  
  splin.state = CP.setProperty(Pin,hin,fluid,"SpecialProperties");
  splin.viscosity = Mu;
  
  Vel = massflow/(rho*A);
  A = (Modelica.Constants.pi/4)*D^2;
  phi = (m_liquid/m_vapor)*sqrt(rho_vapor/rho_liquid);
  Reynolds_no = rho*Vel*D/Mu;
  
  Tin = Tout;
  
  f = if Reynolds_no<2300 then 64/Reynolds_no else 0.25/(log((e/3.7*D)+(5.74/Reynolds_no^0.9)))^2;
  
  delP = (0.5*phi*(f*L/D)*rho*Vel^2) + (rho*Modelica.Constants.g_n*delZ) + (rho*L*der(Vel));
  Pout = Pin - delP;
  
  pipeout.state = CP.setProperty(Pout,Tout,fluid,"pT");
  pipeout.h = hout;
  
  op.P = Pout;
  op.h = hout;
  op.Q = massflow;
annotation(
    Icon(graphics = {Rectangle(origin = {0, -30}, fillColor = {170, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, 30}, {100, -30}}), Rectangle(origin = {0, 30}, fillColor = {255, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, 30}, {100, -30}})}, coordinateSystem(initialScale = 0.1)));
    end TwoPhase_Pipe;
