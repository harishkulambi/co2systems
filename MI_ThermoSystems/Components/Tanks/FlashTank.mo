within MI_ThermoSystems.Components.Tanks;

model FlashTank

  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
    // Fluid naming convention based on coolprop python wrapper
    
  CoolPropLinker.Interfaces.Functions CP;
    //Function to calculate properties
  CoolPropLinker.Interfaces.BaseProperties propin;
    //Properties at inlet
  CoolPropLinker.Interfaces.BaseProperties propL;
    //Properties at liquid outlet
  CoolPropLinker.Interfaces.BaseProperties propV;
    //Properties at vapor outlet
   
   SI.Pressure P "Tank inlet pressure";

   SI.SpecificEnthalpy hin "Tank inlet enthalpy";
   SI.SpecificEnthalpy hliquid "Tank liquid enthalpy";
   SI.SpecificEnthalpy hvapor "Tank vapor enthalpy";
   
   SI.MassFlowRate massflow "Tank inlet mass flow rate";
   SI.MassFlowRate massflow_liquid "Tank liquid mass flow rate";
   SI.MassFlowRate massflow_vapor "Tank vapor mass flow rate";
    
protected 
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort opV annotation(
    Placement(visible = true, transformation(origin = {110, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort opL annotation(
    Placement(visible = true, transformation(origin = {110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation
  
  ip.P = P;
  ip.h = hin;
  ip.Q = massflow;
  
  propin.state = CP.setProperty(P,hin,fluid,"ph");
  
  propL.state = CP.setProperty(P,0,fluid,"pQ");
  propL.h = hliquid;
  
  propV.state = CP.setProperty(P,1,fluid,"pQ");
  propV.h = hvapor;
  
  massflow_vapor = if propin.Q <0 then 0 elseif propin.Q>1 then massflow else massflow*propin.Q;
  massflow = massflow_liquid + massflow_vapor;
  
  opL.P = P;
  opL.h = hliquid;
  opL.Q = massflow_liquid;
  
  opV.P = P;
  opV.h = hvapor;
  opV.Q = massflow_vapor;
  
annotation(
    Icon(graphics = {Rectangle(fillColor = {255, 255, 255}, fillPattern = FillPattern.VerticalCylinder, extent = {{-100, 100}, {100, -100}})}));
    end FlashTank;
