within MI_ThermoSystems.Components.Tanks;

model GasTank1
    
    import SI = Modelica.SIunits;
    
    parameter String fluid = "carbondioxide";
    
    CoolPropLinker.Interfaces.Functions CP;
    
    CoolPropLinker.Interfaces.BaseProperties propin;    // inlet properties
    CoolPropLinker.Interfaces.BaseProperties proptankinitial; // tank initial properties
    CoolPropLinker.Interfaces.BaseProperties propout;    // outlet properties
    CoolPropLinker.Interfaces.SpecialProperties splprop; // special properties of tank
    
    parameter SI.Pressure Ptank_initial = 10e5;
    parameter SI.Temperature Ttank_initial = 50+273.15;
    
    parameter SI.Volume V = 10 "Volume of tank";
    parameter Real R = 188.9 "Gas Constant";
    
   
    SI.Temperature Tin "Inlet Temperature";
    SI.MassFlowRate min "Inlet Mass Flow Rate";
//    parameter SI.SpecificEnthalpy hin = 480e3 "Inlet Specific Enthalpy";
    SI.SpecificEnthalpy hin;
//    SI.MassFlowRate mout "Outlet Mass Flow Rate";
    
    SI.SpecificEnthalpy htank_initial "Initial Tank Specific Enthalpy";
    SI.SpecificHeatCapacityAtConstantVolume Cv "Specific heat capacity";
     
    SI.Pressure Ptank(start = Ptank_initial) "Tank Pressure";
    SI.Temperature Ttank "Tank Temperature";
    SI.SpecificEnthalpy htank "Tank Specific Enthalpy";
    SI.Mass M "Mass of Fluid";
    
    SI.InternalEnergy Uref "Reference Value of Internal Energy";
    SI.InternalEnergy Utank "Tank Internal Energy";
    SI.InternalEnergy Ufinal "Tank Final Internal Energy";
    
    Real ntank;
    Real nfinal;
    
protected      
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation

  ip.P = Ptank;
  ip.h = hin;
  ip.Q = min;
  
  propin.state = CP.setProperty(Ptank,hin,fluid,"ph");
  propin.T = Tin;
  
  proptankinitial.state = CP.setProperty(Ptank_initial,Ttank_initial,fluid,"pT");
  proptankinitial.h = htank_initial;
  
  splprop.state = CP.setProperty(Ptank_initial,htank_initial,fluid,"SpecialProperties");
  splprop.CvMass = Cv;
  
//  min - mout = (V/(R*Ttank_initial))*der(Ptank);
// min  = (V/(R*Ttank_initial))*der(Ptank); 
//min = mout; 
  ntank * Utank + (nfinal - ntank)*hin = nfinal*Ufinal;
    
    ntank  = (Ptank_initial*(1/proptankinitial.d))/(R*Ttank_initial);
    nfinal = (Ptank*V)/(R*Ttank);
    
  hin = Uref + (Ptank/propin.d);
    Utank  = Uref + Cv*(Ttank_initial - Tin);
    Ufinal = Uref + Cv*(-Ttank + Tin);

  propout.state = CP.setProperty(Ptank,Ttank,fluid,"pT");
  propout.h = htank;
  
  M = V*propout.d;
//  RealP = Ptank;
  
  op.P = Ptank;
  op.h = htank;
  op.Q = min;

//  Utank  = Uref + Cv*(-Ttank_initial + Tin);
//    Ufinal = Uref + Cv*(-Ttank + Tin);


       
annotation(
    Icon(graphics = {Rectangle(extent = {{-100, 100}, {100, -100}})}));
    end GasTank1;
