within MI_ThermoSystems.Components.Tanks;

model GasTankController1

  import SI = Modelica.SIunits;
  
  parameter SI.Pressure Pref = 10e5 "Reference Pressure";

protected   
//  Modelica.Blocks.Interfaces.RealInput Psrc annotation(
//    Placement(visible = true, transformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput Ptank annotation(
    Placement(visible = true, transformation(origin = {120, 0}, extent = {{20, -20}, {-20, 20}}, rotation = 0), iconTransformation(origin = {120, 0}, extent = {{20, -20}, {-20, 20}}, rotation = 0)));
  Modelica.Blocks.Interfaces.BooleanOutput InletValve annotation(
    Placement(visible = true, transformation(origin = {-60, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 90), iconTransformation(origin = {-60, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Blocks.Interfaces.BooleanOutput OutletValve annotation(
    Placement(visible = true, transformation(origin = {60, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 90), iconTransformation(origin = {60, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
equation

 when Ptank <= Pref then 
    
        InletValve  = true;
        OutletValve = false;
    
//    elsewhen Ptank >= Psrc then 
    
//        InletValve  = false;
//        OutletValve = true;
 
 end when ;
  
annotation(
    Icon(graphics = {Rectangle(extent = {{-100, 40}, {100, -40}})}));
    end GasTankController1;
