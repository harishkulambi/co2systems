within MI_ThermoSystems.Components.Tanks;

model Merger
  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
    //Fluid naming convention based on coolprop python wrapper
  
  CoolPropLinker.Interfaces.Functions CP;
    //Function to calculate properties
  CoolPropLinker.Interfaces.BaseProperties propout;
    //Properties at merger outlet
    SI.Temperature Tout "Outlet temperature of merger";
  MI_ThermoSystems.Interfaces.InletPort ipV annotation(
    Placement(visible = true, transformation(origin = {-110, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.InletPort ipL annotation(
    Placement(visible = true, transformation(origin = {-106, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  op.P = 0.5*(ipL.P+ipV.P);
  ipL.Q + ipV.Q = op.Q;
  op.h * op.Q = (ipL.h*ipL.Q) + (ipV.h*ipV.Q);
  
  propout.state = CP.setProperty(op.P,op.h,fluid,"ph");
  propout.T = Tout;
  
 
annotation(
    Diagram,
    Icon(graphics = {Rectangle(origin = {0, 50}, fillColor = {255, 255, 255}, fillPattern = FillPattern.VerticalCylinder, extent = {{-100, 50}, {100, -50}}), Rectangle(origin = {0, -50}, fillColor = {170, 255, 255}, fillPattern = FillPattern.VerticalCylinder, extent = {{-100, 50}, {100, -50}})}, coordinateSystem(initialScale = 0.1)));
    end Merger;
