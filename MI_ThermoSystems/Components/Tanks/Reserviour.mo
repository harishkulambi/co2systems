within MI_ThermoSystems.Components.Tanks;

model Reserviour
  
  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
    //Fluid conventin naming based on coolprop python wrapper
  
  CoolPropLinker.Interfaces.Functions CP;
    //Function to calculate properties
    
  CoolPropLinker.Interfaces.BaseProperties propin;
    //Properties at inlet
  
  SI.Pressure Pin "Inlet Pressure";
  SI.Pressure Pout "Outlet Pressure";
  SI.Pressure Pacc(start = 30e5) "Pressure in tank";
  
  SI.Temperature Tacc "Reserviour temperature";
  SI.MassFlowRate min,mout "inlet massflow, outlet massflow";
  
  parameter SI.Volume V = 0.04 "Volume of the tank";
  parameter Real MolecularWeight = 44 "Molecular weight of fluid";
  
protected
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

initial equation 
  der(Pacc) = 0;
  
equation
  ip.P = Pin;
  ip.Q = min;
  ip.h = op.h;
  
  propin.state = CP.setProperty(Pin,ip.h,fluid,"ph");
  propin.T = Tacc;
  
  ((V*MolecularWeight)/(Modelica.Constants.R*Tacc))*der(Pacc) = min - mout;
  Pin = Pacc;
  Pout = Pacc;
  op.P = Pout;
  op.Q = mout;
annotation(
    Icon(graphics = {Ellipse(origin = {0, 90}, fillColor = {255, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, 10}, {100, -10}}, endAngle = 360), Ellipse(origin = {0, -90}, fillColor = {255, 0, 0}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, 10}, {100, -10}}, endAngle = 360), Rectangle(fillColor = {255, 255, 255},fillPattern = FillPattern.VerticalCylinder, extent = {{-100, 90}, {100, -90}})}));
    end Reserviour;
