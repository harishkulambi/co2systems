within MI_ThermoSystems.Components.Tanks;

model Splitter

parameter Real ValveOpening = 1;
Real valveopening;


protected
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op1 annotation(
    Placement(visible = true, transformation(origin = {110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation 

  ip.P = op.P;
  op.P = op1.P;
  
  ip.h = op.h;
  op.h = op1.h;
  

//  ValveOpening = if ValveOpening > 1 then 1 elseif ValveOpening < 0 then 0 else ValveOpening;
//  ValveOpening = max(0,1);

 valveopening = if ValveOpening > 1 then 1 elseif ValveOpening < 0 then  0 else ValveOpening;
   
    
  
  if valveopening == 0 then 
    op.Q = 0;
    op1.Q = 0;
  elseif valveopening ==1 then 
    op.Q = 0.5*ip.Q;
    op1.Q = ip.Q - op.Q;
  else 
    op.Q = valveopening*ip.Q;
    op1.Q = (1- valveopening)*ip.Q;
  end if;
    
annotation(
    Icon(graphics = {Rectangle(origin = {-50, 0}, fillColor = {255, 255, 255}, fillPattern = FillPattern.VerticalCylinder, extent = {{-50, 100}, {50, -100}}), Rectangle(origin = {50, 50}, fillColor = {85, 255, 127}, fillPattern = FillPattern.HorizontalCylinder, extent = {{50, 50}, {-50, -50}}), Rectangle(origin = {50, -50}, fillColor = {0, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{50, -50}, {-50, 50}})}));
    end Splitter;
