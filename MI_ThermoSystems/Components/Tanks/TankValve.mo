within MI_ThermoSystems.Components.Tanks;

model TankValve
  import SI = Modelica.SIunits;
  
  SI.Pressure Pin "Valve Inlet Pressure";
  SI.Pressure Pout "Valve Outlet Pressure";
  
  SI.MassFlowRate min(start = 100)"Inlet Mass Flow Rate";
  parameter Real Cv = 1e-5;
protected   
  Modelica.Blocks.Interfaces.BooleanInput valveopen annotation(
    Placement(visible = true, transformation(origin = {0, 20}, extent = {{-20, -20}, {20, 20}}, rotation = -90), iconTransformation(origin = {0, 20}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-104, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation
  
  ip.P = Pin;
  ip.Q = min;
  
  ip.h = op.h;
  
  if valveopen == true then 
    min = Cv*(Pin - Pout);
  else 
    min = 0;
  end if;
  
  op.P = Pout;
  op.Q = min;
annotation(
    Icon(graphics = {Polygon(points = {{-100, 60}, {-100, -60}, {100, 60}, {100, -60}, {-100, 60}})}));
    end TankValve;
