within MI_ThermoSystems.Components.Turbos_Pumps;

model Reciprocating_Compressor

  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
    //Fluid convention naming based on CoolProp Python wrapper
  CoolPropLinker.Interfaces.Functions CP;
    // Functions to calculate properties
  CoolPropLinker.Interfaces.BaseProperties compin;
    // Properties at the inlet
  CoolPropLinker.Interfaces.BaseProperties compout;
    // Properties at the outlet
  CoolPropLinker.Interfaces.BaseProperties compisen;
    //Properties at isentropic outlet
  SI.Pressure Pin "Compressor inlet pressure";
  
  SI.Temperature Tin "Compressor inlet temperature";
  SI.Temperature Tout "Compressor outlet temperature";

  SI.MassFlowRate massflow "Ideal Mass flow rate in compressor";
  SI.MassFlowRate massflow_actual "Actual mass flow rate in compressor";
  
  SI.SpecificEnthalpy hin "Compressor inlet enthalpy";
  SI.SpecificEnthalpy hout "Compressor outlet enthalpy";
  SI.SpecificEnthalpy hisen "Compressor isentropic enthalpy";
  
  SI.SpecificEntropy sin "Compressor inlet entropy";
  
   SI.Pressure Pout  "Compressor outlet pressure";
  parameter SI.Length L = 0.046 "Stroke length";
  parameter SI.Diameter B = 0.030 "Bore diameter";
  parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm N = 1450 "RPM";
  parameter SI.Frequency f = 50 "Compressor frequency";
  parameter  SI.Frequency fa = 65"Actual compressor frequency";
  parameter Real Qh(unit = "m3/hr") = 6 "Ideal Volume flow rate per hour";
 
  SI.Volume Vs "Swept volume";
  SI.Velocity Vp "Piston velocity";
  SI.VolumeFlowRate Qs "Ideal Volume flow rate per second";
  SI.VolumeFlowRate V "Actual Volume flow";
  SI.Power W "Work delivered to compressor";

  Real N_sps "Number of strokes per second";
  Real eta_isen "Isentropic efficiency of compressor";
  Real eta_V "Volumetric efficiency of compressor";
  parameter Real PR = 1.4"Pressure Ratio";
// Modelica.Blocks.Interfaces.RealInput F annotation(
//    Placement(visible = true, transformation(origin = {-118, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-118, 60}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  
protected  
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation

  ip.P = Pin;
  ip.h = hin;
//  ip.Q = massflow_actual;
//  ip.Q = op.Q;
  Vs    = (Modelica.Constants.pi/4)*B^2*L;
  Vp    = (2*L*N)/60;
  N_sps = Vp/L;
  Qs    = eta_V*(Qh/3600);
//  F = fa;
  compin.state = CP.setProperty(Pin,hin,fluid,"ph");
  compin.T = Tin;
  compin.s = sin;
  
  PR = (Pout/Pin);
  
  massflow = (fa/f)*Qs*compin.d;
  eta_V = 0.005 * (PR)^2 - 0.1224 * (PR) + 1.0643;
  eta_isen = -0.0088 * (PR)^4 + 0.132 * (PR)^3 - 0.723 * (PR)^2 + 1.6698 * (PR)^1 - 0.7084;
  massflow_actual = massflow*eta_V;
  V = eta_V*Qs;
  
  compisen.state = CP.setProperty(Pout,sin,fluid,"ps");
  compisen.h = hisen;
  
  eta_isen = (hisen - hin)/(hout - hin);

  compout.state = CP.setProperty(Pout,hout,fluid,"ph");
  compout.T = Tout;
  
  W = massflow_actual * (hout- hin);
  
  op.P = Pout;
  op.h = hout;
  op.Q = massflow_actual;
  
  
annotation(
    Icon(graphics = {Polygon(points = {{-100, 100}, {-100, -100}, {100, -40}, {100, 40}, {-100, 100}})}, coordinateSystem(initialScale = 0.1)));
    end Reciprocating_Compressor;
