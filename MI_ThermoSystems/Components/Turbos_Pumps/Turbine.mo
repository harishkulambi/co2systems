within MI_ThermoSystems.Components.Turbos_Pumps;

model Turbine

  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
    // Fluid convention naming based on coolprop python wrapper
  CoolPropLinker.Interfaces.Functions CP;
    // Function to calculate properties
  CoolPropLinker.Interfaces.BaseProperties turbine_in;
    // Turbine inlet properties
  CoolPropLinker.Interfaces.BaseProperties turbine_out;
    // Turbine outlet properties
  CoolPropLinker.Interfaces.BaseProperties turbine_isen;
    // Turbine isentropic properties
  SI.Pressure Pin "Turbine inlet pressure";
  SI.Pressure Pout "Turbine outlet pressure";
  
  SI.Temperature Tin "Turbine inlet temperature";
  SI.Temperature Tout "Turbine outlet temperature";
  
  SI.SpecificEnthalpy hin "Turbine inlet enthalpy";
  SI.SpecificEnthalpy hout "Turbine outlet enthalpy";
  SI.SpecificEnthalpy hisen "Turbine isentropic enthalpy";
  
  SI.MassFlowRate massflow "Mass flow of fluid entering the turbine";
  
  SI.SpecificEntropy sin "Turbine inlet entropy";
  SI.Power W_t "Work done by turbine";
  
  parameter  Real PR = 1.6 "Turbine pressure ratio";
  parameter Real eta = 0.953 "Turbine isentropic efficiency";
//  Modelica.Blocks.Interfaces.RealInput Pressure_Ratio annotation(
//    Placement(visible = true, transformation(origin = {-120, 70}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 70}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  

protected
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-106, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation

  ip.P = Pin;
  ip.h = hin;
  ip.Q = massflow;
//  Pressure_Ratio = PR;
  turbine_in.state = CP.setProperty(Pin,hin,fluid,"ph");
  turbine_in.T = Tin;
  turbine_in.s = sin;
  
  PR = Pin/Pout;
  
  turbine_isen.state = CP.setProperty(Pout,sin,fluid,"ps");
  turbine_isen.h = hisen;
  
  eta = (hout - hin)/(hisen - hin);
  
  turbine_out.state = CP.setProperty(Pout,hout,fluid,"ph");
  turbine_out.T = Tout;
  
  W_t = massflow*(hin - hout);
  
  op.P = Pout;
  op.h = hout;
  op.Q = massflow;
  
annotation(
    Icon(graphics = {Polygon(points = {{100, 100}, {100, -100}, {-100, -40}, {-100, 40}, {100, 100}})}));
    end Turbine;
