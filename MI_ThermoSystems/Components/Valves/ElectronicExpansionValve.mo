within MI_ThermoSystems.Components.Valves;

model ElectronicExpansionValve

  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
  
  CoolPropLinker.Interfaces.Functions CP; // Function to calculate the properties
  CoolPropLinker.Interfaces.BaseProperties propin;//Inlet Properties
  CoolPropLinker.Interfaces.BaseProperties propout;//Outlet Properties
  
  SI.Pressure Pin(min = 10e5) "Inlet Pressure";
  SI.Pressure Pout "Outlet  Pressure";
  SI.Pressure delP(start = 20e5, min = 10e5) "Pressure drop";
  
  SI.Temperature Tin "Inlet Temperature";
  SI.Temperature Tout "Outlet Temperature";
  SI.SpecificEnthalpy hin(min = 100e3) "Inlet Enthalpy";
  SI.SpecificEnthalpy hout(min = 100e3) "Outlet Enthalpy";
  
  SI.MassFlowRate massflow "Mass Flow Rate of fluid";
  
  SI.Density rhoin(start= 100) "Inlet density";
  SI.Density rhoout(start=100) "Outlet density";
  
  Real eta "Efficiency";
  parameter Real A = 0.52e-5;
  Modelica.Blocks.Interfaces.RealInput ValveOpening annotation(
    Placement(visible = true, transformation(origin = {0, -20}, extent = {{20, -20}, {-20, 20}}, rotation = -90), iconTransformation(origin = {0, -20}, extent = {{20, -20}, {-20, 20}}, rotation = -90)));
protected 
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation

  ip.P = Pin;
  ip.h = hin;
  ip.Q = massflow;
  
  propin.state = CP.setProperty(Pin,hin,fluid,"ph");
  propin.T = Tin;
  propin.d = rhoin;
  
  delP = massflow^2/(eta^2*A^2*ValveOpening)*(1/(2*rhoin));
  hin = hout;
  eta = 0.02005*sqrt(rhoin) + 0.634/rhoout;
  
  propout.state = CP.setProperty(Pout,hout,fluid,"ph");
  propout.T = Tout;
  propout.d = rhoout;
  
  Pout = Pin-delP;
  
  op.P = Pout;
  op.h = hout;
  op.Q = massflow;
  
annotation(
    Icon(graphics = {Polygon(lineThickness = 0.5, points = {{-100, 60}, {-100, -60}, {100, 60}, {100, -60}, {-100, 60}}), Line(origin = {0, 30}, points = {{0, -30}, {0, 30}}, thickness = 0.5), Rectangle(origin = {0, 63}, fillPattern = FillPattern.Solid, extent = {{-40, 17}, {40, -3}})}, coordinateSystem(initialScale = 0.1)));
    end ElectronicExpansionValve;
