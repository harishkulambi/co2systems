within MI_ThermoSystems.Components.Valves;

model LinearValve
  
  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
    // Fluid convention naming based on CoolProp Python wrapper
  CoolPropLinker.Interfaces.Functions CP;
    // Function to calculate properties
  CoolPropLinker.Interfaces.BaseProperties propin;
    // Valve inlet properties
  CoolPropLinker.Interfaces.BaseProperties propout;
    // Valve outlet properties
  
  SI.Pressure Pin "Valve inlet pressure";
  SI.Pressure Pout "Valve outlet pressure";
  SI.Pressure delP "pressure drop";
  
  SI.Temperature Tin "Valve inlet temperature";
  SI.Temperature Tout "Valve outlet temperature";
  
  SI.SpecificEnthalpy hin "Valve inlet enthalpy";
  SI.SpecificEnthalpy hout "Valve outlet enthalpy";
  
  SI.MassFlowRate min "Valve inlet mass flow rate";
  SI.MassFlowRate mout "Valve outlet mass flow rate";
 
  Real eta "Isentropic efficiency";
    
protected  
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation

annotation(
    Icon(graphics = {Polygon(lineThickness = 0.5, points = {{-100, 80}, {-100, -80}, {100, 80}, {100, -80}, {-100, 80}}), Rectangle(origin = {0, 70}, fillPattern = FillPattern.Solid, extent = {{-20, 10}, {20, -10}}), Line(origin = {0, 30}, points = {{0, 30}, {0, -30}}, thickness = 0.5)}));
    end LinearValve;
