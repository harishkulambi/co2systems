within MI_ThermoSystems.Components.Valves;

model MassFlowSplitter

parameter Real ValveOpening = 1 ;
Real VO;


protected 
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {0, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op1 annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation

  ip.P = op.P;
  op.P = op1.P;
  
  ip.h = op.h;
  op.h = op1.h;
  
  VO = if ValveOpening > 1 then 1 elseif ValveOpening < 0 then 0 else ValveOpening;
  
  if VO == 0 then 
      op.Q = 0;
      op1.Q = 0;
    elseif VO == 1 then
      op.Q = 0.5*ip.Q;
      op1.Q = ip.Q - op.Q;
    else 
      op.Q = VO*ip.Q;
      op1.Q = (1-VO)*ip.Q;
  end if;         
annotation(
    Icon(graphics = {Polygon(lineThickness = 0.5, points = {{-100, 60}, {-100, -60}, {100, 60}, {100, -60}, {-100, 60}}), Rectangle(origin = {0, 70}, fillPattern = FillPattern.Solid, extent = {{-40, 10}, {40, -10}}), Line(origin = {0, 30}, points = {{0, -30}, {0, 30}}, thickness = 0.5)}));
    
    
    
    
    end MassFlowSplitter;
