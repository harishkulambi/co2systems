within MI_ThermoSystems;

package Components
  extends Modelica.Icons.Package;

  
  annotation(
    Icon(graphics = {Rectangle(origin = {8, 19}, fillColor = {255, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-66, 41}, {54, -79}}), Rectangle(origin = {-79, 0}, fillColor = {255, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-21, 40}, {21, -40}}), Rectangle(origin = {81, 0}, fillColor = {255, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{19, 40}, {-19, -40}})}));
  
end Components;
