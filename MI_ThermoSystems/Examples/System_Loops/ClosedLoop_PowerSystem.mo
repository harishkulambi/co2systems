within MI_ThermoSystems.Examples.System_Loops;

model ClosedLoop_PowerSystem
  extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Turbos_Pumps.Reciprocating_Compressor reciprocating_Compressor1(PR = 1.6, Qh = 45)  annotation(
    Placement(visible = true, transformation(origin = {-56, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX gasketed_PHX1(A = 187, Lc = 2, Tout_primary(start = 373.15), Tout_secondary(start = 1073.15),co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide", fluid2 = "carbondioxide")  annotation(
    Placement(visible = true, transformation(origin = {70, 6}, extent = {{-36, -36}, {36, 36}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.Heater heater1 annotation(
    Placement(visible = true, transformation(origin = {156, 36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Turbos_Pumps.Turbine turbine1 annotation(
    Placement(visible = true, transformation(origin = {202, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Cooler_Gasketed_PHX cooler_Gasketed_PHX1(A = 50, Lc = 0.1, Tout_secondary(start = 328.15), co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide")  annotation(
    Placement(visible = true, transformation(origin = {-32, 80}, extent = {{-24, -24}, {24, 24}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 2, T0 = 298.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-73, 39}, extent = {{-5, -5}, {5, 5}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {13, 85}, extent = {{-5, -5}, {5, 5}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Node_PQ node_PQ1(P0 = 7.5e+06)  annotation(
    Placement(visible = true, transformation(origin = {-290, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.GasTank gasTank1 annotation(
    Placement(visible = true, transformation(origin = {-222, -36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.TankValve tankValve1 annotation(
    Placement(visible = true, transformation(origin = {-280, -44}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.TankValve tankValve2 annotation(
    Placement(visible = true, transformation(origin = {-174, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.GasTankController gasTankController1 annotation(
    Placement(visible = true, transformation(origin = {-224, -98}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(gasTankController1.Ptank, gasTank1.RealP) annotation(
    Line(points = {{-212, -98}, {-211, -98}, {-211, -98}, {-210, -98}, {-210, -28}, {-210, -28}, {-210, -29}, {-210, -29}, {-210, -28}}, color = {0, 0, 127}));
  connect(tankValve1.valveopen, gasTankController1.InletValve) annotation(
    Line(points = {{-280, -42}, {-280, -42}, {-280, -94}, {-230, -94}, {-230, -94}, {-230, -94}, {-230, -92}}, color = {255, 0, 255}));
  connect(gasTankController1.OutletValve, tankValve2.valveopen) annotation(
    Line(points = {{-218, -93}, {-196, -93}, {-196, -91}, {-174, -91}, {-174, -37}, {-171, -37}, {-171, -39}, {-174, -39}}, color = {255, 0, 255}));
  connect(gasTank1.op, tankValve2.ip) annotation(
    Line(points = {{-211.4, -36}, {-198.4, -36}, {-198.4, -36}, {-185.4, -36}, {-185.4, -40}, {-183.4, -40}, {-183.4, -40}, {-183.4, -40}}));
  connect(tankValve2.op, reciprocating_Compressor1.ip) annotation(
    Line(points = {{-163.4, -40}, {-67.4, -40}, {-67.4, 8}, {-65.4, 8}}));
  connect(tankValve1.op, gasTank1.ip) annotation(
    Line(points = {{-269.4, -44}, {-233.4, -44}, {-233.4, -36}, {-231.4, -36}}));
  connect(node_PQ1.ip, tankValve1.ip) annotation(
    Line(points = {{-300.6, 14}, {-318.6, 14}, {-318.6, -44}, {-288.6, -44}, {-288.6, -44}, {-290.6, -44}, {-290.6, -44}}));
  connect(node_PQ1.ip, cooler_Gasketed_PHX1.hotout) annotation(
    Line(points = {{-300.6, 14}, {-245.6, 14}, {-245.6, 62}, {-63.6, 62}, {-63.6, 61}, {-63.6, 61}, {-63.6, 60}}));
  connect(cooler_Gasketed_PHX1.coldout, sink1.ip) annotation(
    Line(points = {{-0.8, 99.2}, {4.2, 99.2}, {4.2, 101.2}, {9.2, 101.2}, {9.2, 87.2}, {7.2, 87.2}, {7.2, 85.2}, {7.2, 85.2}}));
  connect(source_PQT1.op, cooler_Gasketed_PHX1.coldin) annotation(
    Line(points = {{-67.7, 39}, {-65.7, 39}, {-65.7, 41}, {-63.7, 41}, {-63.7, 57}, {-62.7, 57}, {-62.7, 55}, {-63.7, 55}}));
  connect(cooler_Gasketed_PHX1.hotin, gasketed_PHX1.hotout) annotation(
    Line(points = {{-0.8, 104}, {11.2, 104}, {11.2, 104}, {25.2, 104}, {25.2, -22}, {24.2, -22}, {24.2, -22}, {23.2, -22}}));
  connect(heater1.op, turbine1.ip) annotation(
    Line(points = {{167, 36}, {179, 36}, {179, 36}, {193, 36}, {193, 8}, {193, 8}, {193, 8}, {191, 8}}));
  connect(turbine1.op, gasketed_PHX1.hotin) annotation(
    Line(points = {{212.6, 8}, {222.6, 8}, {222.6, 8}, {232.6, 8}, {232.6, 68}, {116.6, 68}, {116.6, 42}, {116.6, 42}}));
  connect(gasketed_PHX1.coldout, heater1.ip) annotation(
    Line(points = {{116.8, 34.8}, {131.8, 34.8}, {131.8, 34.8}, {146.8, 34.8}, {146.8, 36.8}, {145.8, 36.8}, {145.8, 36.8}, {144.8, 36.8}}));
  connect(reciprocating_Compressor1.op, gasketed_PHX1.coldin) annotation(
    Line(points = {{-45.4, 8}, {-7.4, 8}, {-7.4, -32}, {24.6, -32}, {24.6, -31}, {24.6, -31}, {24.6, -30}}));
  annotation(
    Diagram(coordinateSystem(extent = {{-250, -200}, {250, 200}})),
    Icon(coordinateSystem(extent = {{-250, -200}, {250, 200}})),
    __OpenModelica_commandLineOptions = "");end ClosedLoop_PowerSystem;
