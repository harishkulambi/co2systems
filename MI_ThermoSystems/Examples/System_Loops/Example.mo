within MI_ThermoSystems.Examples.System_Loops;

model Example
extends Modelica.Icons.Example;


  MI_ThermoSystems.Components.Turbos_Pumps.Reciprocating_Compressor reciprocating_Compressor1(PR = 1.6, Qh = 45)  annotation(
    Placement(visible = true, transformation(origin = {-110, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Turbos_Pumps.Turbine turbine1 annotation(
    Placement(visible = true, transformation(origin = {110, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX gasketed_PHX1(A = 187, Lc = 2, Tout_primary(start = 373.15), Tout_secondary(start = 1073.15),co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide", fluid2 = "carbondioxide")  annotation(
    Placement(visible = true, transformation(origin = {-1, -13}, extent = {{-19, -19}, {19, 19}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.Heater heater1 annotation(
    Placement(visible = true, transformation(origin = {62, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 2, T0 = 298.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-86, 6}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-34, 24}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Node_PQ node_PQ1(P0 = 7.5e+06)  annotation(
    Placement(visible = true, transformation(origin = {-100, 28}, extent = {{6, -6}, {-6, 6}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Cooler_Gasketed_PHX cooler_Gasketed_PHX1( A = 50, Lc = 0.1,Tout_secondary(start = 328.15), co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide", fluid2 = "water")  annotation(
    Placement(visible = true, transformation(origin = {-64, 36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(source_PQT1.op, cooler_Gasketed_PHX1.coldin) annotation(
    Line(points = {{-79.64, 6}, {-77.64, 6}, {-77.64, 26}, {-77.64, 26}}));
  connect(cooler_Gasketed_PHX1.coldout, sink1.ip) annotation(
    Line(points = {{-51, 44}, {-39, 44}, {-39, 24}, {-41, 24}, {-41, 24}, {-41, 24}}));
  connect(cooler_Gasketed_PHX1.hotin, gasketed_PHX1.hotout) annotation(
    Line(points = {{-51, 46}, {-38, 46}, {-38, 46}, {-25, 46}, {-25, -28}, {-27, -28}, {-27, -28}, {-27, -28}}));
  connect(cooler_Gasketed_PHX1.hotout, node_PQ1.ip) annotation(
    Line(points = {{-77, 28}, {-93, 28}}));
  connect(node_PQ1.op, reciprocating_Compressor1.ip) annotation(
    Line(points = {{-106.36, 28}, {-122.36, 28}, {-122.36, -10}}));
  connect(gasketed_PHX1.coldout, heater1.ip) annotation(
    Line(points = {{23.7, 2.2}, {31.7, 2.2}, {31.7, 2.2}, {39.7, 2.2}, {39.7, 10.2}, {49.7, 10.2}, {49.7, 10.2}}));
  connect(heater1.op, turbine1.ip) annotation(
    Line(points = {{73, 10}, {101, 10}, {101, -10}, {99, -10}}));
  connect(reciprocating_Compressor1.op, gasketed_PHX1.coldin) annotation(
    Line(points = {{-99.4, -10}, {-79.4, -10}, {-79.4, -10}, {-59.4, -10}, {-59.4, -32}, {-25.4, -32}, {-25.4, -32}}));
  connect(turbine1.op, gasketed_PHX1.hotin) annotation(
    Line(points = {{120.6, -10}, {120.6, -10}, {120.6, 30}, {24.6, 30}, {24.6, 6}, {24.6, 6}, {24.6, 6}, {24.6, 6}}));
  annotation(
    Diagram(coordinateSystem(extent = {{-150, -100}, {150, 100}}, initialScale = 0.1), graphics = {Text(origin = {-108, -30}, extent = {{-16, 4}, {16, -4}}, textString = "Compressor"), Text(origin = {-4, -43}, extent = {{-22, 7}, {22, -7}}, textString = "Recuperator"), Text(origin = {61, -9}, extent = {{-15, 3}, {15, -3}}, textString = "Heater"), Text(origin = {110, -26}, extent = {{-12, 4}, {12, -4}}, textString = "Turbine"), Text(origin = {-61, 19}, extent = {{-11, 5}, {7, -5}}, textString = "Cooler"), Text(origin = {-99, 43}, extent = {{-9, 1}, {9, -5}}, textString = "Node")}),
    Icon(coordinateSystem(extent = {{-150, -100}, {150, 100}})),
    __OpenModelica_commandLineOptions = "");end Example;
