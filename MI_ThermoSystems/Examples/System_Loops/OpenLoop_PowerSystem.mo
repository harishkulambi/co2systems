within MI_ThermoSystems.Examples.System_Loops;

model OpenLoop_PowerSystem

extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Turbos_Pumps.Reciprocating_Compressor reciprocating_Compressor1(PR = 1.6, Qh = 45)  annotation(
    Placement(visible = true, transformation(origin = {-44, -4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX Recuperator(A = 187, Lc = 2,co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide", fluid2 = "carbondioxide")  annotation(
    Placement(visible = true, transformation(origin = {73, -3}, extent = {{-19, -19}, {19, 19}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.Heater heater1 annotation(
    Placement(visible = true, transformation(origin = {152, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Turbos_Pumps.Turbine turbine1 annotation(
    Placement(visible = true, transformation(origin = {192, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Cooler_Gasketed_PHX cooler_Gasketed_PHX1(A = 50, Lc = 0.1, Tout_secondary(start = 328.15),Tout_primary = 343.15, co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide")  annotation(
    Placement(visible = true, transformation(origin = {-21, 73}, extent = {{-25, -25}, {25, 25}}, rotation = 0)));
  
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 2, T0 = 298.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-72, 24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {28, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.GasTank gasTank1(Ptank_initial = 1e+06,Ttank_initial = 353.15, V = 3)  annotation(
    Placement(visible = true, transformation(origin = {-150, -28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.GasTankController gasTankController1(Pref = 1e+06)  annotation(
    Placement(visible = true, transformation(origin = {-160, -68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PT_1 source_PT_11(P0 = 7.6e+06, T0 = 303.15)  annotation(
    Placement(visible = true, transformation(origin = {-196, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.TankValve tankValve1 annotation(
    Placement(visible = true, transformation(origin = {-104, -28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.TankValve tankValve2 annotation(
    Placement(visible = true, transformation(origin = {-168, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink_P sink_P1(P0 = 7.6e+06)  annotation(
    Placement(visible = true, transformation(origin = {-94, 118}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(sink_P1.ip, cooler_Gasketed_PHX1.hotout) annotation(
    Line(points = {{-83.4, 118}, {-53.4, 118}, {-53.4, 54}, {-53.4, 54}}));
  connect(source_PT_11.op, tankValve2.ip) annotation(
    Line(points = {{-185.2, -24}, {-177.2, -24}, {-177.2, -17}, {-177.2, -17}, {-177.2, -10}}));
  connect(tankValve2.op, gasTank1.ip) annotation(
    Line(points = {{-157.4, -10}, {-157.9, -10}, {-157.9, -10}, {-160.4, -10}, {-160.4, -19}, {-160.4, -19}, {-160.4, -28}}));
  connect(tankValve2.valveopen, gasTankController1.InletValve) annotation(
    Line(points = {{-168, -8}, {-168, -15}, {-170, -15}, {-170, -22}, {-168, -22}, {-168, -42}, {-166, -42}, {-166, -62}}, color = {255, 0, 255}));
  connect(gasTank1.op, tankValve1.ip) annotation(
    Line(points = {{-139.4, -28}, {-126.4, -28}, {-126.4, -28}, {-117.4, -28}, {-117.4, -28}, {-113.4, -28}, {-113.4, -28}, {-113.4, -28}}));
  connect(tankValve1.op, reciprocating_Compressor1.ip) annotation(
    Line(points = {{-93.4, -28}, {-55.4, -28}, {-55.4, -4}, {-53.4, -4}}));
  connect(gasTankController1.OutletValve, tankValve1.valveopen) annotation(
    Line(points = {{-154, -63}, {-129, -63}, {-129, -61}, {-104, -61}, {-104, -25}, {-104, -25}, {-104, -27}}, color = {255, 0, 255}));
  connect(source_PT_11.Pout, gasTankController1.Psrc) annotation(
    Line(points = {{-185, -16.6}, {-179, -16.6}, {-179, -16.6}, {-171, -16.6}, {-171, -68.6}, {-173, -68.6}, {-173, -68.6}, {-173, -68.6}}, color = {0, 0, 127}));
  connect(gasTank1.RealP, gasTankController1.Ptank) annotation(
    Line(points = {{-139, -20}, {-143, -20}, {-143, -20}, {-149, -20}, {-149, -68}, {-147, -68}, {-147, -68}, {-149, -68}}, color = {0, 0, 127}));
  connect(gasTankController1.Ptank, gasTank1.RealP) annotation(
    Line(points = {{-148, -68}, {-138, -68}, {-138, -20}, {-138, -20}}, color = {0, 0, 127}));
  connect(cooler_Gasketed_PHX1.coldout, sink2.ip) annotation(
    Line(points = {{11.5, 93}, {16.5, 93}, {16.5, 95}, {19.5, 95}, {19.5, 75}, {17.5, 75}, {17.5, 73}, {17.5, 73}}));
  connect(source_PQT1.op, cooler_Gasketed_PHX1.coldin) annotation(
    Line(points = {{-61.4, 24}, {-53.4, 24}, {-53.4, 48}, {-53.4, 48}}));
  connect(cooler_Gasketed_PHX1.hotin, Recuperator.hotout) annotation(
    Line(points = {{11.5, 98}, {30.5, 98}, {30.5, 98}, {47.5, 98}, {47.5, -18}, {49.5, -18}, {49.5, -18}, {47.5, -18}}));
  connect(heater1.op, turbine1.ip) annotation(
    Line(points = {{163, 4}, {173, 4}, {173, 4}, {179, 4}, {179, 4}, {181, 4}, {181, 4}, {181, 4}}));
  connect(turbine1.op, Recuperator.hotin) annotation(
    Line(points = {{202.6, 4}, {212.6, 4}, {212.6, 4}, {218.6, 4}, {218.6, 36}, {96.6, 36}, {96.6, 16}, {99.6, 16}, {99.6, 16}, {98.6, 16}}));
  connect(Recuperator.coldout, heater1.ip) annotation(
    Line(points = {{97.7, 12.2}, {118.7, 12.2}, {118.7, 12.2}, {141.7, 12.2}, {141.7, 4.2}, {140.7, 4.2}, {140.7, 4.2}, {139.7, 4.2}}));
  connect(reciprocating_Compressor1.op, Recuperator.coldin) annotation(
    Line(points = {{-33.4, -4}, {26.6, -4}, {26.6, -22}, {46.6, -22}, {46.6, -22}, {48.6, -22}, {48.6, -22}}));
  annotation(
    Diagram(coordinateSystem(extent = {{-200, -150}, {200, 150}})),
    Icon(coordinateSystem(extent = {{-200, -150}, {200, 150}})),
    __OpenModelica_commandLineOptions = "");
        
    end OpenLoop_PowerSystem;
