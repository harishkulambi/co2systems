within MI_ThermoSystems.Examples.System_Loops;

model PowerSystem
  extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Turbos_Pumps.Reciprocating_Compressor reciprocating_Compressor1(PR = 1.6, Qh = 45) annotation(
    Placement(visible = true, transformation(origin = {-50, 31}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Turbos_Pumps.Turbine turbine1 annotation(
    Placement(visible = true, transformation(origin = {89, -25}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.Heater heater1 annotation(
    Placement(visible = true, transformation(origin = {89, 35}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX Recuperator(A = 187, Lc = 2, Tout_primary(start = 373.15), Tout_secondary(start = 1073.15), co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide", fluid2 = "carbondioxide") annotation(
    Placement(visible = true, transformation(origin = {19, 34}, extent = {{-25, 25}, {25, -25}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX Cooler(A = 50,Tout_primary(start = 269.15), Tout_secondary(start = 328.15),co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide", fluid2 = "water")  annotation(
    Placement(visible = true, transformation(origin = {-49, -24}, extent = {{-11, 11}, {11, -11}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 2, T0 = 298.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-76, 0}, extent = {{-5, -5}, {5, 5}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-26, -20}, extent = {{-5, -5}, {5, 5}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Node_PQ node_PQ1(P0 = 7.5e+06, T0 = 343.15)  annotation(
    Placement(visible = true, transformation(origin = {-75.5, 34.5}, extent = {{-5.5, -5.5}, {5.5, 5.5}}, rotation = 0)));
  Modelica.Blocks.Sources.Step step1(height = 10, offset = 70 + 273.15, startTime = 2000)  annotation(
    Placement(visible = true, transformation(origin = {-110, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(reciprocating_Compressor1.op, Recuperator.coldin) annotation(
    Line(points = {{-39, 31}, {-30.4, 31}, {-30.4, 59}, {-13.9, 59}}));
  connect(node_PQ1.op, reciprocating_Compressor1.ip) annotation(
    Line(points = {{-69.67, 34.5}, {-69.67, 36}, {-65.67, 36}, {-65.67, 31}, {-61, 31}}));
  connect(step1.y, node_PQ1.InT) annotation(
    Line(points = {{-99, 22}, {-75, 22}, {-75, 28}, {-75, 28}}, color = {0, 0, 127}));
  connect(Cooler.hotout, node_PQ1.ip) annotation(
    Line(points = {{-63.3, -15.2}, {-77.3, -15.2}, {-77.3, -14.2}, {-91.3, -14.2}, {-91.3, 34.8}, {-81.3, 34.8}, {-81.3, 34.8}, {-81.3, 34.8}, {-81.3, 34.8}}));
  connect(Cooler.coldout, sink2.ip) annotation(
    Line(points = {{-34.7, -32.8}, {-33.7, -32.8}, {-33.7, -32.8}, {-32.7, -32.8}, {-32.7, -32.8}, {-30.7, -32.8}, {-30.7, -20.8}, {-30.7, -20.8}, {-30.7, -19.8}, {-30.7, -19.8}, {-30.7, -19.8}, {-30.7, -19.8}}));
  connect(source_PQT1.op, Cooler.coldin) annotation(
    Line(points = {{-70.7, 0}, {-67.2, 0}, {-67.2, 0}, {-63.7, 0}, {-63.7, -13}, {-62.7, -13}, {-62.7, -13}, {-62.7, -13}}));
  connect(Cooler.hotin, Recuperator.hotout) annotation(
    Line(points = {{-34.7, -35}, {-23.7, -35}, {-23.7, -34}, {-12.7, -34}, {-12.7, 10}, {-12.7, 10}, {-12.7, 54}}));
  connect(Recuperator.coldout, heater1.ip) annotation(
    Line(points = {{51.5, 14}, {69, 14}, {69, 36}, {73.5, 36}, {73.5, 35}, {78, 35}}));
  connect(turbine1.op, Recuperator.hotin) annotation(
    Line(points = {{78.4, -25}, {73.9, -25}, {73.9, -25}, {69.4, -25}, {69.4, 8}, {52.4, 8}, {52.4, 8.5}, {52.4, 8.5}, {52.4, 8.75}, {52.4, 8.75}, {52.4, 9}}));
  connect(heater1.op, turbine1.ip) annotation(
    Line(points = {{100, 35}, {104.5, 35}, {104.5, 36}, {109, 36}, {109, -24}, {100, -24}, {100, -23.5}, {100, -23.5}, {100, -24.75}, {100, -24.75}, {100, -25}}));
  annotation(
    Diagram(coordinateSystem(extent = {{-150, -100}, {150, 100}}, grid = {1, 1})),
    Icon(coordinateSystem(extent = {{-150, -100}, {150, 100}}, grid = {1, 1})),
    __OpenModelica_commandLineOptions = "");
end PowerSystem;
