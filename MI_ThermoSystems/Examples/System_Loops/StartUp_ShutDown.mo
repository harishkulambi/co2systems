within MI_ThermoSystems.Examples.System_Loops;

model StartUp_ShutDown/*, T = 303.15*/
  extends Modelica.Icons.Example;
  MI_ThermoSystems.BoundaryConditions.Source_PT_1 source_PT_11(P0 = 7.6e+06, T0 = 303.15) annotation(
    Placement(visible = true, transformation(origin = {-190, -90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Components.Tanks.TankValve tankValve1 annotation(
    Placement(visible = true, transformation(origin = {-160, -80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.GasTank gasTank1(Ptank_initial = 1e+06, Ttank_initial = 353.15, V = 3) annotation(
    Placement(visible = true, transformation(origin = {-130, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.GasTankController gasTankController1 annotation(
    Placement(visible = true, transformation(origin = {-130, -96}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Components.Tanks.TankValve tankValve2 annotation(
    Placement(visible = true, transformation(origin = {-100, -80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Turbos_Pumps.Reciprocating_Compressor reciprocating_Compressor1(PR = 1.6, Qh = 45) annotation(
    Placement(visible = true, transformation(origin = {-70, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX gasketed_PHX1(A = 187, Lc = 2, co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide", fluid2 = "carbondioxide") annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.Heater heater1 annotation(
    Placement(visible = true, transformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Turbos_Pumps.Turbine turbine1 annotation(
    Placement(visible = true, transformation(origin = {90, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Cooler_Gasketed_PHX cooler_Gasketed_PHX1(  A = 50, Lc = 0.1, Tout_primary(start = 393.15), Tout_secondary(start = 328.15), co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide") annotation(
    Placement(visible = true, transformation(origin = {-75, 41}, extent = {{-19, -19}, {19, 19}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1(fluid = "water") annotation(
    Placement(visible = true, transformation(origin = {-34, 34}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 2, T0 = 298.15, fluid = "water") annotation(
    Placement(visible = true, transformation(origin = {-118, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BoundaryConditions.Sink_P sink_P1(P0 = 7.6e+06) annotation(
    Placement(visible = true, transformation(origin = {-188, 36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.GasTank1 gasTank11(Ptank_initial = 1e+06, Ttank_initial = 303.15, V = 3)  annotation(
    Placement(visible = true, transformation(origin = {-138, 52}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(sink_P1.ip, gasTank11.op) annotation(
    Line(points = {{-178, 36}, {-150, 36}, {-150, 52}, {-148, 52}}));
  connect(gasTank11.ip, cooler_Gasketed_PHX1.hotout) annotation(
    Line(points = {{-128, 52}, {-98, 52}, {-98, 26}, {-100, 26}}));
  connect(source_PQT1.op, cooler_Gasketed_PHX1.coldin) annotation(
    Line(points = {{-108, 4}, {-100, 4}, {-100, 22}, {-100, 22}}));
  connect(cooler_Gasketed_PHX1.coldout, sink1.ip) annotation(
    Line(points = {{-50, 56}, {-40, 56}, {-40, 34}, {-40, 34}}));
  connect(cooler_Gasketed_PHX1.hotin, gasketed_PHX1.hotout) annotation(
    Line(points = {{-50, 60}, {-26, 60}, {-26, -16}, {-26, -16}}));
  connect(gasketed_PHX1.hotin, turbine1.op) annotation(
    Line(points = {{26, 20}, {110, 20}, {110, -30}, {100, -30}, {100, -30}}));
  connect(heater1.op, turbine1.ip) annotation(
    Line(points = {{62, 0}, {70, 0}, {70, -30}, {80, -30}, {80, -30}}));
  connect(gasketed_PHX1.coldout, heater1.ip) annotation(
    Line(points = {{26, 16}, {32, 16}, {32, 0}, {38, 0}, {38, 0}}));
  connect(reciprocating_Compressor1.op, gasketed_PHX1.coldin) annotation(
    Line(points = {{-60, -30}, {-40, -30}, {-40, -20}, {-26, -20}, {-26, -20}}));
  connect(tankValve2.op, reciprocating_Compressor1.ip) annotation(
    Line(points = {{-90, -80}, {-86, -80}, {-86, -30}, {-82, -30}, {-82, -30}}));
  connect(gasTank1.RealP, gasTankController1.Ptank) annotation(
    Line(points = {{-118, -42}, {-114, -42}, {-114, -96}, {-118, -96}, {-118, -96}}, color = {0, 0, 127}));
  connect(source_PT_11.Pout, gasTankController1.Psrc) annotation(
    Line(points = {{-178, -82}, {-176, -82}, {-176, -96}, {-142, -96}, {-142, -96}}, color = {0, 0, 127}));
  connect(gasTankController1.OutletValve, tankValve2.valveopen) annotation(
    Line(points = {{-124, -90}, {-100, -90}, {-100, -78}, {-100, -78}}, color = {255, 0, 255}));
  connect(tankValve1.valveopen, gasTankController1.InletValve) annotation(
    Line(points = {{-160, -78}, {-160, -78}, {-160, -90}, {-136, -90}, {-136, -90}}, color = {255, 0, 255}));
  connect(gasTank1.op, tankValve2.ip) annotation(
    Line(points = {{-120, -50}, {-116, -50}, {-116, -80}, {-110, -80}, {-110, -80}}));
  connect(tankValve1.op, gasTank1.ip) annotation(
    Line(points = {{-150, -80}, {-146, -80}, {-146, -50}, {-140, -50}, {-140, -50}}));
  connect(source_PT_11.op, tankValve1.ip) annotation(
    Line(points = {{-180, -90}, {-170, -90}, {-170, -80}, {-170, -80}}));
  annotation(
    Diagram(coordinateSystem(extent = {{-200, -100}, {200, 100}})),
    Icon(coordinateSystem(extent = {{-200, -100}, {200, 100}})),
    __OpenModelica_commandLineOptions = "");
end StartUp_ShutDown;
