within MI_ThermoSystems.Examples.System_Loops;

model trial
  MI_ThermoSystems.Components.Tanks.GasTank1 gasTank11(Ptank_initial = 1e+06, Ttank_initial = 303.15, V = 1)  annotation(
    Placement(visible = true, transformation(origin = {-104, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Cooler_Gasketed_PHX cooler_Gasketed_PHX1(A = 50, Lc = 0.2, Tout_primary(start = 393.15), Tout_secondary(start = 328.15), co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide", fluid2 = "water")  annotation(
    Placement(visible = true, transformation(origin = {-18, 22}, extent = {{-28, -28}, {28, 28}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1 annotation(
    Placement(visible = true, transformation(origin = {-152, 6}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {58, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 2, T0 = 298.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-120, -54}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT2(P0 = 7.6e+06, Q0 = 2.16, T0 = 573.15)  annotation(
    Placement(visible = true, transformation(origin = {74, 70}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(cooler_Gasketed_PHX1.hotin, source_PQT2.op) annotation(
    Line(points = {{18, 50}, {64, 50}, {64, 70}, {64, 70}}));
  connect(source_PQT1.op, cooler_Gasketed_PHX1.coldin) annotation(
    Line(points = {{-110, -54}, {-54, -54}, {-54, -6}, {-54, -6}}));
  connect(cooler_Gasketed_PHX1.coldout, sink2.ip) annotation(
    Line(points = {{18, 44}, {48, 44}, {48, -24}, {48, -24}}));
  connect(sink1.ip, gasTank11.op) annotation(
    Line(points = {{-142, 6}, {-116, 6}, {-116, 0}, {-114, 0}}));
  connect(gasTank11.ip, cooler_Gasketed_PHX1.hotout) annotation(
    Line(points = {{-94, 0}, {-54, 0}, {-54, 0}, {-54, 0}}));
end trial;
