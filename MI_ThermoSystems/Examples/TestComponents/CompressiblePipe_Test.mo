within MI_ThermoSystems.Examples.TestComponents;

model CompressiblePipe_Test
extends Modelica.Icons.Example;
import SI = Modelica.SIunits;
  SI.Pressure PressureDrop;

  MI_ThermoSystems.Components.Pipes.Compressible_Pipe compressible_Pipe1(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-50, -50}, {50, 50}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 1, T0 = 338.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-90, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {90, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation
  PressureDrop = compressible_Pipe1.delP;
  connect(compressible_Pipe1.op, sink1.ip) annotation(
    Line(points = {{56, 0}, {68, 0}, {68, -70}, {80, -70}, {80, -70}}));
  connect(source_PQT1.op, compressible_Pipe1.ip) annotation(
    Line(points = {{-78, 70}, {-72, 70}, {-72, 0}, {-56, 0}, {-56, 0}}));
end CompressiblePipe_Test;
