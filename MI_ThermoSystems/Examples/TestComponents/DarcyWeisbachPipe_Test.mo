within MI_ThermoSystems.Examples.TestComponents;

model DarcyWeisbachPipe_Test
extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Pipes.DarcyWeisbach_Pipe darcyWeisbach_Pipe(D = 25e-3)  annotation(
    Placement(visible = true, transformation(origin = {1.37668e-14, -3.9968e-15}, extent = {{-100, -100}, {100, 100}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT(P0 = 3.2e+06, Q0 = 1.5, T0 = 269.961)  annotation(
    Placement(visible = true, transformation(origin = {-162, 80}, extent = {{-22, -22}, {22, 22}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink annotation(
    Placement(visible = true, transformation(origin = {181, -79}, extent = {{21, -21}, {-21, 21}}, rotation = 0)));
equation
  connect(darcyWeisbach_Pipe.op, sink.ip) annotation(
    Line(points = {{110, -2}, {142, -2}, {142, -80}, {158, -80}, {158, -80}}));
  connect(source_PQT.op, darcyWeisbach_Pipe.ip) annotation(
    Line(points = {{-138, 80}, {-128, 80}, {-128, 0}, {-110, 0}, {-110, 0}}));
end DarcyWeisbachPipe_Test;
