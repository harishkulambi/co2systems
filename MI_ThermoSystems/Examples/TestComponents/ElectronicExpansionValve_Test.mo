within MI_ThermoSystems.Examples.TestComponents;

model ElectronicExpansionValve_Test

extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Valves.ElectronicExpansionValve electronicExpansionValve1(A = 1.5e-5)  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-32, -32}, {32, 32}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 1e+07, Q0 = 0.2, T0 = 318.15)  annotation(
    Placement(visible = true, transformation(origin = {-70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1 annotation(
    Placement(visible = true, transformation(origin = {70, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.RealExpression realExpression1(y = 0.5)  annotation(
    Placement(visible = true, transformation(origin = {-16, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(realExpression1.y, electronicExpansionValve1.ValveOpening) annotation(
    Line(points = {{-4, -40}, {0, -40}, {0, -6}, {0, -6}}, color = {0, 0, 127}));
  connect(electronicExpansionValve1.op, sink1.ip) annotation(
    Line(points = {{36, 0}, {60, 0}, {60, 0}, {60, 0}}));
  connect(source_PQT1.op, electronicExpansionValve1.ip) annotation(
    Line(points = {{-60, 0}, {-38, 0}, {-38, 0}, {-36, 0}}));
end ElectronicExpansionValve_Test;
