within MI_ThermoSystems.Examples.TestComponents;

model FinTubeHeatExchanger_Test
extends Modelica.Icons.Example;
  MI_ThermoSystems.HeatExchangers.AirCooled_HeatExchanger.FinTube_HeatExchanger finTube_HeatExchanger1 annotation(
    Placement(visible = true, transformation(origin = {7.10543e-15, 0}, extent = {{-60, -60}, {60, 60}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 1.0054e+07, Q0 = 617.63 / 3600, T0 = 328.69)  annotation(
    Placement(visible = true, transformation(origin = {-90, -1.77636e-15}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1 annotation(
    Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PT source_PT1(P0 = 150000, T0 = 305.15, fluid = "air")  annotation(
    Placement(visible = true, transformation(origin = {0, -70}, extent = {{10, -10}, {-10, 10}}, rotation = -90)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2(fluid = "air")  annotation(
    Placement(visible = true, transformation(origin = {0, 70}, extent = {{10, -10}, {-10, 10}}, rotation = -90)));
equation
  connect(finTube_HeatExchanger1.secondaryoutlet, sink2.ip) annotation(
    Line(points = {{0, 42}, {0, 42}, {0, 60}, {0, 60}}));
  connect(source_PT1.op, finTube_HeatExchanger1.secondaryinlet) annotation(
    Line(points = {{0, -60}, {0, -60}, {0, -42}, {0, -42}}));
  connect(finTube_HeatExchanger1.primaryoutlet, sink1.ip) annotation(
    Line(points = {{66, 0}, {78, 0}, {78, 0}, {80, 0}}));
  connect(source_PQT1.op, finTube_HeatExchanger1.primaryinlet) annotation(
    Line(points = {{-80, 0}, {-70, 0}, {-70, 0}, {-66, 0}}));
end FinTubeHeatExchanger_Test;
