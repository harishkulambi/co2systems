within MI_ThermoSystems.Examples.TestComponents;

model FlashTank_Test
extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Tanks.FlashTank flashTank annotation(
    Placement(visible = true, transformation(origin = {1, 1}, extent = {{-41, -41}, {41, 41}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PHQ source_PHQ1(H0 = 250e3, P0 = 4e+06, Q0 = 1.2)  annotation(
    Placement(visible = true, transformation(origin = {-86, 84}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1 annotation(
    Placement(visible = true, transformation(origin = {84,80}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2 annotation(
    Placement(visible = true, transformation(origin = {84, -80}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(flashTank.opL, sink2.ip) annotation(
    Line(points = {{46, -28}, {60, -28}, {60, -80}, {73, -80}}));
  connect(flashTank.opV, sink1.ip) annotation(
    Line(points = {{46, 30}, {60, 30}, {60, 80}, {73, 80}}));
  connect(source_PHQ1.op, flashTank.ip) annotation(
    Line(points = {{-76, 84}, {-60, 84}, {-60, 0}, {-44, 0}, {-44, 2}}));
end FlashTank_Test;
