within MI_ThermoSystems.Examples.TestComponents;

model GasTank_Test
  extends Modelica.Icons.Example;
  MI_ThermoSystems.BoundaryConditions.Source_PT_1 source_PT_11(P0 = 1e+07, T0 = 373.15)  annotation(
    Placement(visible = true, transformation(origin = {-74, 34}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.GasTank gasTank1(Ptank_initial = 1e+06, Ttank_initial = 323.15, V = 3)  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.TankValve tankValve1(Cv = 1e-5)  annotation(
    Placement(visible = true, transformation(origin = {-40, 8.88178e-16}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.TankValve tankValve2(Cv = 1e-5)  annotation(
    Placement(visible = true, transformation(origin = {40, 0}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink_P sink_P1(P0 = 519000)  annotation(
    Placement(visible = true, transformation(origin = {74, -26}, extent = {{6, -6}, {-6, 6}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.GasTankController gasTankController1(Pref = 1e+06)  annotation(
    Placement(visible = true, transformation(origin = {0, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(tankValve2.valveopen, gasTankController1.OutletValve) annotation(
    Line(points = {{40, 2}, {40, 2}, {40, -26}, {6, -26}, {6, -34}, {6, -34}}, color = {255, 0, 255}));
  connect(tankValve1.valveopen, gasTankController1.InletValve) annotation(
    Line(points = {{-40, 2}, {-40, 2}, {-40, -26}, {-6, -26}, {-6, -34}, {-6, -34}}, color = {255, 0, 255}));
  connect(gasTank1.RealP, gasTankController1.Ptank) annotation(
    Line(points = {{22, 16}, {28, 16}, {28, -40}, {12, -40}, {12, -40}}, color = {0, 0, 127}));
  connect(source_PT_11.Pout, gasTankController1.Psrc) annotation(
    Line(points = {{-68, 38}, {-54, 38}, {-54, -40}, {-12, -40}, {-12, -40}}, color = {0, 0, 127}));
  connect(sink_P1.ip, tankValve2.op) annotation(
    Line(points = {{68, -26}, {60, -26}, {60, 0}, {46, 0}, {46, 0}}));
  connect(gasTank1.op, tankValve2.ip) annotation(
    Line(points = {{22, 0}, {34, 0}, {34, 0}, {34, 0}}));
  connect(tankValve1.op, gasTank1.ip) annotation(
    Line(points = {{-34, 0}, {-22, 0}, {-22, 0}, {-22, 0}}));
  connect(source_PT_11.op, tankValve1.ip) annotation(
    Line(points = {{-68, 34}, {-60, 34}, {-60, 0}, {-46, 0}, {-46, 0}}));
end GasTank_Test;
