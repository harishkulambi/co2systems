within MI_ThermoSystems.Examples.TestComponents;

model Heater_Test
extends Modelica.Icons.Example;
  BoundaryConditions.Source_PQT source_PQT1(P0 = 1.2e+07, Q0 = 2, T0 = 628.15)  annotation(
    Placement(visible = true, transformation(origin = {-80, 36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BoundaryConditions.Sink sink1 annotation(
    Placement(visible = true, transformation(origin = {70, -36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.Heater heater1 annotation(
    Placement(visible = true, transformation(origin = { -3, 1}, extent = {{-45, -45}, {45, 45}}, rotation = 0)));
equation
  connect(heater1.op, sink1.ip) annotation(
    Line(points = {{46, 2}, {60, 2}, {60, -36}, {60, -36}}));
  connect(source_PQT1.op, heater1.ip) annotation(
    Line(points = {{-70, 36}, {-50, 36}, {-50, 2}, {-52, 2}}));
end Heater_Test;
