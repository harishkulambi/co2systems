within MI_ThermoSystems.Examples.TestComponents;

model IncompressiblePipe_Test
extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Pipes.Incompressible_Pipe incompressible_Pipe(D = 0.2, e = 0.45, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-1.42109e-14, 7.10543e-15}, extent = {{-100, -100}, {100, 100}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT(P0 = 400000, Q0 = 1.5, T0 = 298.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-162, 80}, extent = {{-22, -22}, {22, 22}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {160, -80}, extent = {{20, -20}, {-20, 20}}, rotation = 0)));
equation
  connect(source_PQT.op, incompressible_Pipe.ip) annotation(
    Line(points = {{-138, 80}, {-126, 80}, {-126, 0}, {-106, 0}, {-106, 0}}));
  connect(incompressible_Pipe.op, sink.ip) annotation(
    Line(points = {{106, 0}, {130, 0}, {130, -80}, {138, -80}, {138, -80}}));
end IncompressiblePipe_Test;
