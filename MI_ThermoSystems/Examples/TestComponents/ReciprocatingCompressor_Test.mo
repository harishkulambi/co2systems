within MI_ThermoSystems.Examples.TestComponents;

model ReciprocatingCompressor_Test
extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Turbos_Pumps.Reciprocating_Compressor reciprocating_Compressor1(PR = 1.6, fa = 50)  annotation(
    Placement(visible = true, transformation(origin = {9, 5}, extent = {{-59, -59}, {59, 59}}, rotation = 0)));
  BoundaryConditions.Source_PT source_PT1(P0 = 6e+06, T0 = 303.15)  annotation(
    Placement(visible = true, transformation(origin = {-132, 66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BoundaryConditions.Sink sink1 annotation(
    Placement(visible = true, transformation(origin = {118, -52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(reciprocating_Compressor1.op, sink1.ip) annotation(
    Line(points = {{72, 5}, {108, 5}, {108, -52}}));
  connect(source_PT1.op, reciprocating_Compressor1.ip) annotation(
    Line(points = {{-122, 66}, {-58, 66}, {-58, 5}, {-56, 5}}));
end ReciprocatingCompressor_Test;
