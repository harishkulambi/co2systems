within MI_ThermoSystems.Examples.TestComponents;

model Reserviour_Test
extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Tanks.Reserviour reserviour1 annotation(
    Placement(visible = true, transformation(origin = {-1, 1}, extent = {{-51, -51}, {51, 51}}, rotation = 0)));
  BoundaryConditions.Source_PQT source_PQT1(P0 = 4e+06, Q0 = 1.3, T0 = 279.15)  annotation(
    Placement(visible = true, transformation(origin = {-86, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1 annotation(
    Placement(visible = true, transformation(origin = {90, -80}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(reserviour1.op, sink1.ip) annotation(
    Line(points = {{54, 0}, {72, 0}, {72, -80}, {80, -80}, {80, -80}, {80, -80}}));
  connect(source_PQT1.op, reserviour1.ip) annotation(
    Line(points = {{-74, 80}, {-68, 80}, {-68, 0}, {-56, 0}, {-56, 0}}));
end Reserviour_Test;
