within MI_ThermoSystems.Examples.TestComponents;

model Turbine_Test
extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Turbos_Pumps.Turbine turbine annotation(
    Placement(visible = true, transformation(origin = {1, -1}, extent = {{-37, -37}, {37, 37}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT(P0 = 2e+07, Q0 = 175.6, T0 = 823.15)  annotation(
    Placement(visible = true, transformation(origin = {-86, 74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink annotation(
    Placement(visible = true, transformation(origin = {84, -80}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(turbine.op, sink.ip) annotation(
    Line(points = {{40, 0}, {60, 0}, {60, -80}, {74, -80}, {74, -80}}));
  connect(source_PQT.op, turbine.ip) annotation(
    Line(points = {{-74, 74}, {-60, 74}, {-60, 0}, {-38, 0}, {-38, 0}}));
end Turbine_Test;
