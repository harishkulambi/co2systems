within MI_ThermoSystems.Examples.TestComponents;

model TwoPhasePipe_Test
extends Modelica.Icons.Example;
  MI_ThermoSystems.Components.Pipes.TwoPhase_Pipe twoPhase_Pipe annotation(
    Placement(visible = true, transformation(origin = {-7.99361e-15, -3.55271e-15}, extent = {{-100, -100}, {100, 100}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PHQ source_PHQ(H0 = 230e3, P0 = 3.5e+06, Q0 = 1.5)  annotation(
    Placement(visible = true, transformation(origin = {-160, 80}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink annotation(
    Placement(visible = true, transformation(origin = {180, -80}, extent = {{20, -20}, {-20, 20}}, rotation = 0)));
equation
  connect(twoPhase_Pipe.op, sink.ip) annotation(
    Line(points = {{106, 0}, {126, 0}, {126, -80}, {159, -80}}));
  connect(source_PHQ.op, twoPhase_Pipe.ip) annotation(
    Line(points = {{-138, 80}, {-130, 80}, {-130, 0}, {-106, 0}}));
end TwoPhasePipe_Test;
