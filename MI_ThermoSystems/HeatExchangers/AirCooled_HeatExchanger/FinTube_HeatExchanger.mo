within MI_ThermoSystems.HeatExchangers.AirCooled_HeatExchanger;

model FinTube_HeatExchanger

  import SI = Modelica.SIunits;
  
  parameter String fluid1 = "carbondioxide";
  parameter String fluid2 = "air";
  
  CoolPropLinker.Interfaces.Functions CP; //Function to calculate properties
  
//Fin Tube Geometry

  parameter SI.Area       A       = 255.9 "Heat transfer area";
  parameter SI.Length     L       = 1.2 "Length of the tube";
  parameter SI.Diameter   D_root  = 12e-3 "Finned root tube diameter";
  parameter SI.Diameter   D_fan   = 910e-3 "Fan diameter";
  parameter SI.Breadth    W       = 599e-3;
  parameter SI.Height     h_fin   = 20e-3 "Fin Height";
  parameter SI.Thickness  t_tube  = 0.7e-3 "Tube thickness";
  parameter SI.Thickness  t_fin   = 0.15e-3 "Fin thickness";
  parameter Real pitch(unit = "m")= 2.2e-3 "Fin pitch";
  parameter Real P_t(unit = "m")  = 60e-3 "Transverse tube pitch";
  parameter Real eta              = 0.8 "Fin efficiency";
  parameter Real Np               = 30 "Number of tube passes";
  parameter Real Ke               = 1;
  parameter Real Kc               = 1;
  
  SI.Area A_min "Minimum flow area in the tube bank";
  SI.Area A_face "Face area of the tube bank";
  SI.Area A_ot "Total outside surface area";
  SI.Area A_of "Outside finned area";
  SI.Diameter Di "Inside tube diameter";
  SI.Diameter D_fin "Fin outside diameter";
  
  Real Nr "Number of tube rows";
  Real Nf "Number of fins";
  Real Nt "Number of tubes per row";
  Real Surface_Enlargement_Factor;
  Real Ntubes "Total number of tubes";
  
//Tube side variables

  CoolPropLinker.Interfaces.BaseProperties primaryin; // Properties at the inlet of primary fluid
  CoolPropLinker.Interfaces.BaseProperties primaryout;// Properties at the outlet of primary fluid
  CoolPropLinker.Interfaces.SpecialProperties splprimary;//Special properties of the primary fluid
  
  SI.Pressure Pin_primary "Primary fluid inlet pressure";
  SI.Pressure Pout_primary "Primary fluid outlet pressure";
  SI.Pressure delP_primary "Primary fluid pressure drop";
  
  SI.Temperature Tin_primary "Primary fluid inlet temperature";
  SI.Temperature Tout_primary(start = 30+273.15) "Primary fluid outlet temperature";
  
  SI.SpecificEnthalpy hin_primary "Primary fluid inlet enthaply";
  SI.SpecificEnthalpy hout_primary "Primary fluid outlet enthalpy";
  
  SI.MassFlowRate min_primary "Primary fluid inlet mass flow rate";
  SI.MassFlowRate mout_primary "Primary fluid outlet mass flow rate";
  
  SI.SpecificHeatCapacityAtConstantPressure Cp_primary "Primary fluid heat capacity";
  SI.ThermalConductivity K_primary "Primary fluid thermal conductivity";
  SI.PrandtlNumber Pr_primary "Primary fluid prandtl number";
  SI.DynamicViscosity Mu_primary "Primary fluid viscosity";
  
  SI.CoefficientOfHeatTransfer h_primary "Primary fluid heat transfer coefficient";
  SI.Area A_tube "Tube area";
  
  Real G_primary(unit = "kg/sm^2") "Mass velocity";
  Real Re_primary "Primary fluid reynolds number";
  Real Nu_primary "Primary fluid Nusselt number";
  Real f_primary "Primary fluid friction factor";
  
  parameter SI.ThermalConductivity K_tube = 16.3 "SS 316 Thermal Conductivity";
  
//Fin side variables

  CoolPropLinker.Interfaces.BaseProperties secondaryin;//Secondary fluid inlet properties
  CoolPropLinker.Interfaces.BaseProperties secondaryout;//Secondary fluid outlet properties
  CoolPropLinker.Interfaces.SpecialProperties splsecondary;//Secondary fluid special properties
  
  SI.Pressure Pin_secondary"Secondary fluid inlet pressure";
  SI.Pressure Pout_secondary "Secondary fluid outlet pressure";
  SI.Pressure delP_secondary "Secondary fluid pressure drop";
  
  SI.Temperature Tin_secondary "Secondary fluid inlet temperature";
  SI.Temperature Tout_secondary "Seconadry fluid outlet temperature";
  
  SI.SpecificEnthalpy hin_secondary "Secondary fluid inlet enthalpy";
  SI.SpecificEnthalpy hout_secondary "Secondary fluid outlet enthalpy";
  
  SI.MassFlowRate min_secondary "Secondary fluid inlet mass flow rate";
  SI.MassFlowRate mout_secondary "Secondary fluid outlet mass flow rate";
  
  SI.Velocity V "Velocity of air";
  parameter SI.Frequency f_rated = 50 "Maximum frequency of fan";
  parameter SI.Frequency f = 50 "Working frequency";
  parameter SI.VolumeFlowRate V_secondary = 23000/3600 "Volume flow rate of air";
  
  SI.SpecificHeatCapacityAtConstantPressure Cp_secondary "Secondary fluid heat capacity";
  SI.ThermalConductivity K_secondary "Secondary fluid thermal conductivity";
  SI.DynamicViscosity Mu_secondary "Secondary fluid viscosity";
  SI.PrandtlNumber Pr_secondary "Secondary fluid prandtl number";
  
  SI.CoefficientOfHeatTransfer ha;
  SI.CoefficientOfHeatTransfer ho;
  
  SI.Diameter Dh "Hydraulic diameter";
  Real Jh "Colburn Modulus";
  Real Re_secondary "Air reynolds number";
  Real G_secondary(unit = "kg/sm^2") "Air mass velocity";
  Real f_secondary "Secondary fluid fricition factor";
 
  SI.CoefficientOfHeatTransfer U "Overall heat transfer coefficient";
//  Real LMTD "Log Mean Temperature Difference";
  
  parameter Real Rfi = 0.0001754 "Refrigerant Fouling";
  parameter Real Rfo = 0.0003525 "Air Fouling";
  
  SI.Power Q "Heat Load";
     

protected 
  MI_ThermoSystems.Interfaces.InletPort primaryinlet annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort primaryoutlet annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.InletPort secondaryinlet annotation(
    Placement(visible = true, transformation(origin = {0, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort secondaryoutlet annotation(
    Placement(visible = true, transformation(origin = {0, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));



equation
//Geometry
  A = ceil(Nr)*Nt*L*A_ot;
    A_ot = (Modelica.Constants.pi*D_root*(1-(t_fin*ceil(Nf)))) + A_of;
    A_of = (2*ceil(Nf)*(Modelica.Constants.pi/4)*(D_fin^2 - D_root^2)) + (Modelica.Constants.pi*D_fin*L*ceil(Nf));
  
  pitch = (L/(Nf-1)) - t_fin;
    
  D_fin = D_root + (2*h_fin);
  
  A_min = ceil(Nt)*L*(P_t-D_root-(2*ceil(Nf)*h_fin*t_fin));
  A_face = W*L;
  
  Nr = ((L-D_fin)/(P_t/D_root)) + 1;  
  
  Surface_Enlargement_Factor = A_min/A_face;
  
  Di = D_root-(2*t_tube);
  Ntubes = (2*Np)+1;
  
//Tube side calculations

  primaryinlet.P = Pin_primary;
  primaryinlet.h = hin_primary;
  primaryinlet.Q = min_primary;
  
  primaryin.state = CP.setProperty(Pin_primary,hin_primary,fluid1,"ph");
  primaryin.T = Tin_primary;
  
  splprimary.state = CP.setProperty(Pin_primary,hin_primary,fluid1,"SpecialProperties");
  splprimary.CpMass = Cp_primary;
  splprimary.Conductivity = K_primary;
  splprimary.viscosity = Mu_primary;
  splprimary.Prandtl = Pr_primary;
  
  A_tube = (Modelica.Constants.pi/4)*Di^2;
  G_primary = min_primary/(A_tube*Ntubes);
  Re_primary = (G_primary*Di)/Mu_primary;
  Nu_primary = 0.023*(Re_primary^0.8)*(Pr_primary^0.3);
  Nu_primary = h_primary*Di/K_primary;
    
  Pin_primary = Pout_primary;
  min_primary = mout_primary;
  
  primaryout.state = CP.setProperty(Pout_primary,Tout_primary,fluid1,"pT");
  primaryout.h = hout_primary;
  
  primaryoutlet.P = Pout_primary;
  primaryoutlet.h = hout_primary;
  primaryoutlet.Q = mout_primary;
  
  f_primary = 1.7*(Re_primary)^(-0.5);
  
  delP_primary = (G_primary^2/(2*primaryin.d))*(((f_primary*A*Ntubes*primaryin.d)/(A_min*primaryin.d)));
  
  // + ((1 + Surface_Enlargement_Factor^2) * ((primaryin.d /primaryout.d) - 1)));
  
  
//Fin side equations

  secondaryinlet.P = Pin_secondary;
  secondaryinlet.h = hin_secondary;
  secondaryinlet.Q = min_secondary;
  
  Pin_secondary = Pout_secondary;
  min_secondary = mout_secondary;
  
  secondaryin.state = CP.setProperty(Pin_secondary,hin_secondary,fluid2,"ph");
  secondaryin.T = Tin_secondary;
  
  splsecondary.state = CP.setProperty(Pin_secondary,hin_secondary,fluid2,"SpecialProperties");
  splsecondary.Conductivity = K_secondary;
  splsecondary.CpMass = Cp_secondary;
  splsecondary.viscosity = Mu_secondary;
  splsecondary.Prandtl = Pr_secondary;
  
  min_secondary = (f/f_rated)*V_secondary*secondaryin.d;
  min_secondary = secondaryin.d*((Modelica.Constants.pi/4)*D_fan^2)*V;
  
  ha = ho*(1-(1-eta)*(A_of/A_ot));
  ho = Jh*G_secondary*Cp_secondary*Pr_secondary^(-2/3);
  Jh = 0.19*Re_secondary^(-0.35);
  G_secondary = (secondaryin.d*V)/(Surface_Enlargement_Factor);
  Re_secondary = G_secondary*Dh/Mu_secondary;
  Dh = 4*L*(A_min/A);
  
  secondaryout.state = CP.setProperty(Pout_secondary,Tout_secondary,fluid2,"pT");
  secondaryout.h = hout_secondary;
  
  f_secondary = 1.7*Re_secondary^(-0.5);
  
  delP_secondary = (G_secondary^2/(2*secondaryin.d))*((Kc+1-Surface_Enlargement_Factor^2)+(f_secondary*A));
  
  U = (((1/h_primary)+Rfi) + (A_ot/(Modelica.Constants.pi*Di))) + ((A_ot*log(D_root/Di))/(2*Modelica.Constants.pi*K_tube));
  
  (min_primary*Cp_primary*(Tin_primary - Tout_primary)) - (min_secondary*Cp_secondary*(Tin_secondary-Tout_secondary)) = 0.002945*primaryin.d*Cp_primary*der(Tout_primary);
  
  Q = min_primary*Cp_primary*(Tin_primary - Tout_primary);
  Q = min_secondary*Cp_secondary*(Tout_secondary-Tin_secondary);
  
  secondaryoutlet.P = Pout_secondary;
  secondaryoutlet.h = hout_secondary;
  secondaryoutlet.Q = mout_secondary;
  
  
  
annotation(
    Diagram(graphics = {Rectangle(extent = {{-100, 60}, {100, -60}})}),
    Icon(graphics = {Rectangle(extent = {{-100, 60}, {100, -60}}), Ellipse(extent = {{-40, 40}, {40, -40}}, endAngle = 360), Ellipse(origin = {0, 20}, fillColor = {85, 170, 255}, fillPattern = FillPattern.VerticalCylinder, extent = {{-4, 20}, {4, -20}}, endAngle = 360), Ellipse(origin = {20, 0}, rotation = -90, fillColor = {85, 170, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-4, 20}, {4, -20}}, endAngle = 360), Ellipse(origin = {-20, 0}, rotation = -90, fillColor = {85, 170, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-4, 20}, {4, -20}}, endAngle = 360), Ellipse(origin = {0, -20}, fillColor = {85, 170, 255}, fillPattern = FillPattern.VerticalCylinder, extent = {{-4, 20}, {4, -20}}, endAngle = 360), Ellipse(fillPattern = FillPattern.Solid, extent = {{-6, 6}, {6, -6}}, endAngle = 360)}));
    end FinTube_HeatExchanger;
