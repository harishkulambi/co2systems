within MI_ThermoSystems.HeatExchangers;

model Heater

  import SI = Modelica.SIunits;
  
  parameter String fluid = "carbondioxide";
  
    CoolPropLinker.Interfaces.Functions CP;
    CoolPropLinker.Interfaces.BaseProperties heaterin;
    CoolPropLinker.Interfaces.SpecialProperties splheaterin;
    CoolPropLinker.Interfaces.BaseProperties heaterout;
    

SI.Pressure Pin "Heater inlet pressure";
SI.Temperature Tin "Heater inlet temperature";
SI.MassFlowRate min "Heater inlet mass flow rate";
SI.SpecificEnthalpy hin "Heater inlet specific enthalpy";
SI.SpecificHeatCapacityAtConstantPressure Cp ;

SI.Pressure Pout "Heater outlet temperature";
SI.Temperature Tout "Heater outlet temperature";
SI.SpecificEnthalpy hout "Heater outlet specific enthalpy";

parameter SI.Temperature Tamb = 20+273.15;

parameter SI.Voltage V = 430;
SI.Current I ;
SI.Resistance R;
parameter SI.Resistance R0 = 0.17 "Copper Resistance";
parameter SI.Power P = 3e3 "Electric power";




protected
  MI_ThermoSystems.Interfaces.InletPort ip annotation(
    Placement(visible = true, transformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort op annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
ip.P = Pin;
ip.h = hin;
ip.Q = min;

Pin = Pout;

heaterin.state = CP.setProperty(Pin,hin,fluid,"ph");
heaterin.T = Tin;

P = V*I;
P = R*I^2;

splheaterin.state = CP.setProperty(Pin,hin,fluid,"SpecialProperties");
splheaterin.CpMass = Cp;

Cp*der(Tout) = ((Tin - Tout)/R) + ((Tamb - Tout)/R0) + P;
//Cp*der(Tout) = ((Tin-Tout)/R) + P;
heaterout.state = CP.setProperty(Pout,Tout,fluid,"pT");
heaterout.h = hout;
op.P = Pout;
op.h = hout;
op.Q = min;

annotation(
    Icon(graphics = {Rectangle(extent = {{-100, 80}, {100, -80}}), Line(origin = {-70, 0}, points = {{-30, 0}, {30, 0}}, color = {255, 0, 0}, thickness = 0.75), Line(origin = {29.87, 0}, points = {{-69.8675, 0}, {-49.8675, 60}, {-9.86754, -60}, {10.1325, 0}, {70.1325, 0}}, color = {255, 0, 0}, thickness = 0.75), Text(origin = {24, 89}, extent = {{-86, 11}, {52, -5}}, textString = "Tout = % Tout K")}, coordinateSystem(initialScale = 0.1)),
    Diagram);
    end Heater;
