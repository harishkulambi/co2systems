within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger;

model Cooler_Gasketed_PHX
  import SI = Modelica.SIunits;
  type corelation = enumeration(Savostin_Tikhonov "Savostin_Tikhonov", Tovazhnyanski "Tovazhnyanski", Chisholm_Wanniarachchi "Chisholm_Wanniarachchi", Talik "Talik", Muley_Manglik "Muley_Manglik");
  //-------------------------------------------------------------------------------------------//
  //COOLPROP PROPERTIES AND FUNCTIONS
  parameter String fluid1 = "water";
  //Primary Fluid convention naming based on coolprop python wrapper
  parameter String fluid2 = "water";
  //Secondary Fluid convention naming based on coolprop python wrapper
  CoolPropLinker.Interfaces.Functions CP;
  //Functions to calculate properties
  CoolPropLinker.Interfaces.BaseProperties primaryin;
  //Properties at the inlet of primary fluid
  CoolPropLinker.Interfaces.BaseProperties primaryout;
  //Properties at the outlet of primary fluid
  CoolPropLinker.Interfaces.SpecialProperties splprimaryin;
  //Special properties at the inlet of primary fluid
  CoolPropLinker.Interfaces.SpecialProperties splprimaryout;
  //Special proprties at the outlet of primary fluid
  CoolPropLinker.Interfaces.BaseProperties secondaryin;
  //Properties at the inlet of secondary fluid
  CoolPropLinker.Interfaces.BaseProperties secondaryout;
  //Properties at the outlet of secondary fluid
  CoolPropLinker.Interfaces.SpecialProperties splsecondaryin;
  //Special properties at the inlet of secondary fluid
  CoolPropLinker.Interfaces.SpecialProperties splsecondaryout;
  //Special properties at the outlet of secondary fluid
  //-------------------------------------------------------------------------------------------//
  parameter Gasketed_PHX.corelation co_relation;
  //GEOMETRY
  parameter SI.Length Lv = 1.55 "Vertical port length";
  parameter SI.Length Lh = 0.43 "Horizontal port length";
  parameter SI.Length Lc = 0.38 "Compressed plate length";
  parameter SI.Thickness t = 0.6e-3 "Thickness of the plate";
  parameter SI.Diameter Dp = 200e-3 "Port diameter";
  parameter SI.Area A = 110 "Effective heat transfer area";
  parameter SI.Conductivity k = 17.5 "Material thermal conductivity SS304";
  parameter Real SEF = 1.25 "Surface enlargement factor";
  parameter Real Np = 1 "Number of passes";
  parameter Real beta = 60 "Chervon angle";
  parameter SI.Volume V = 1 "Volume of fluid";
  SI.Area A_developed "Single plate heat transfer area";
  SI.Area A_projected "Projected plate area";
  SI.Length Lp "Vertical port distance";
  SI.Length Lw "Horizontal port distance";
  Real N_effective "Effective number of plates";
  Real Nt "Total number of plates";
  Real Ncp "Number of channel passes";
  SI.Length b "Mean channel flow gap";
  Real P "Plate pitch/Outside depth of corrugated plate";
  SI.Diameter Dh "Hydraulic diameter";
  SI.Area Ach "Channel flow area";
  //FLUID PROPERTIES
  //HOT FLUID
  SI.Pressure Pin_primary "water inlet pressure";
  SI.Temperature Tin_primary "water inlet temperature";
  SI.MassFlowRate min_primary "waste water stream mass flow rate";
  SI.Temperature Tout_primary(start = 15 + 273.15) "waste water stream outlet temperature";
//  parameter SI.Temperature Tout_primary = 70+273.15;
// SI.Temperature Tout_primary ;
  //COLD FLUID
  SI.Pressure Pin_secondary "cold water inlet pressure";
  SI.Temperature Tin_secondary "cold water inlet temperature";
  SI.MassFlowRate min_secondary "cold water outlet mass flow rate";
  SI.Temperature Tout_secondary(start = 15 + 273.15) "cold water outlet temperature";
  // HEAT TRANSFER CALCULATIONS
  SI.MassFlowRate mch_primary "waste water stream channel flow rate";
  SI.MassFlowRate mch_secondary "cold water channel mass flow rate";
  Real G_primary(unit = "kg/sm^2") "waste water stream mass velocity";
  Real G_secondary(unit = "kg/sm^2") "cold water mass velocity";
  Real Re_primary "Waste water stream reynolds number";
  Real Re_secondary "cold water stream reynolds number";
  Real f_primary "waste water stream frictional factor";
  Real f_secondary "cold water frictional factor";
  Real Nu_primary "waste water nusselt number";
  Real Nu_secondary "cold water nusselt number";
  Real Prandtl_avg "Average prandtl number of both fluids";
  Real viscosity_avg "Average dynamic viscosity of  both fluids";
  SI.CoefficientOfHeatTransfer h_primary "waste water stream heat transfer";
  SI.CoefficientOfHeatTransfer h_secondary "cold water stream heat transfer";
  SI.CoefficientOfHeatTransfer U_clean "Overall heat transfer coefficient under clean condtions";
  SI.CoefficientOfHeatTransfer U_fouled "Overall heat transfer coefficient under fouled conditins";
  Real CF "Cleanliness factor";
  SI.Power Qr "Rated Heat duty";
  SI.Power Qf "Fouled heat duty";
  Real CS "Safety factor";
  //PRESSURE DROP
  SI.Pressure delPc_primary "waste water stream channel pressure drop";
  SI.Pressure delPp_primary "waste water stream port pressure drop";
  SI.Pressure delP_primary "waste water stream total pressure drop";
  SI.Pressure delPc_secondary "cold water channel pressure drop";
  SI.Pressure delPp_secondary "cold water port pressure drop";
  SI.Pressure delP_secondary "cold water total pressure drop";
  Real f_p "Frictional factor";
  Real f_s "Frictional factor";
  constant Real Kp = 0.760 "Emperical constants";
  constant Real m = 0.215 "Emperical constants";
  Real Gc_primary "waste water stream channel mass velocity";
  Real Gc_secondary "cold water channel mass velocity";
  Real Mu_primary "Viscosity average of waste water stream";
  Real Mu_secondary "Viscosity average of cold water";
  Real Mu "Average viscosity of both fluids";
  Real Gp_primary "Port mass velocity";
  Real Gp_secondary "Port mass velocity";
protected
  MI_ThermoSystems.Interfaces.InletPort coldin annotation(
    Placement(visible = true, transformation(origin = {-110, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-130, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort hotout annotation(
    Placement(visible = true, transformation(origin = {-110, -78}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-130, -80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.OutletPort coldout annotation(
    Placement(visible = true, transformation(origin = {110, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {130, 80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Interfaces.InletPort hotin annotation(
    Placement(visible = true, transformation(origin = {110, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {130, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  coldin.P = Pin_secondary;
  coldin.Q = min_secondary;
  
  coldout.P = Pin_secondary;
  coldout.Q = min_secondary;
  
  hotin.P = Pin_primary;
  hotin.Q = min_primary;
  
  hotout.P = Pin_primary;
  hotout.Q = min_primary;
  
//Geometry calculations
  SEF = A_developed / A_projected;
  A_projected = Lp * Lw;
  Lp = Lv - Dp;
  Lw = Lh + Dp;
  A_developed = A / N_effective;
  Nt = ceil(N_effective) + 2;
  Ncp = (Nt - 1) / (2 * Np);
  b = P - t;
  P = Lc / Nt;
  Dh = 2 * b / SEF;
  Ach = b * Lw;
//FLUID PROPERTIES
  primaryin.state = CP.setProperty(Pin_primary, hotin.h, fluid1, "ph");
  primaryin.T = Tin_primary;
//primaryin.h = hotin.h;
  splprimaryin.state = CP.setProperty(Pin_primary, primaryin.h, fluid1, "SpecialProperties");
  
  primaryout.state = CP.setProperty(Pin_primary,hotout.h,fluid1,"ph");
  primaryout.T = Tout_primary;
//  primaryout.h = hotout.h;
  
//  primaryout.state = CP.setProperty(Pin_primary, Tout_primary, fluid1, "pT");
//  primaryout.h = hotout.h;
  splprimaryout.state = CP.setProperty(Pin_primary, primaryout.h, fluid1, "SpecialProperties");
  secondaryin.state = CP.setProperty(Pin_secondary, coldin.h, fluid2, "ph");
  secondaryin.T = Tin_secondary;
  splsecondaryin.state = CP.setProperty(Pin_secondary, secondaryin.h, fluid2, "SpecialProperties");
  secondaryout.state = CP.setProperty(Pin_secondary, Tout_secondary, fluid2, "pT");
  secondaryout.h = coldout.h;
  splsecondaryout.state = CP.setProperty(Pin_secondary, secondaryout.h, fluid2, "SpecialProperties");
//HEAT TRANSFER CALCULATIONS
  mch_primary = min_primary / Ncp;
  G_primary = mch_primary / Ach;
  mch_secondary = min_secondary / Ncp;
  G_secondary = mch_secondary / Ach;
  Re_primary = G_primary * Dh / splprimaryin.viscosity;
  Re_secondary = G_secondary * Dh / splsecondaryin.viscosity;
  Prandtl_avg = (splprimaryin.Prandtl + splsecondaryin.Prandtl) / 2;
  viscosity_avg = (splprimaryin.viscosity + splsecondaryin.viscosity) / 2;
  if co_relation == Gasketed_PHX.corelation.Savostin_Tikhonov then
    {f_primary, Nu_primary} = Corelations.Savostin_Tikhonov(Re_primary, beta, SEF, splprimaryin.Prandtl);
  elseif co_relation == Gasketed_PHX.corelation.Tovazhnyanski then
    {f_primary, Nu_primary} = Corelations.Tovazhnyanski(Re_primary, beta, splprimaryin.Prandtl, Prandtl_avg);
  elseif co_relation == Gasketed_PHX.corelation.Chisholm_Wanniarachchi then
    {f_primary, Nu_primary} = Corelations.Chisholm_Wanniarachchi(Re_primary, beta, SEF, splprimaryin.Prandtl);
  elseif co_relation == Gasketed_PHX.corelation.Talik then
    {f_primary, Nu_primary} = Corelations.Talik(Re_primary, beta, splprimaryin.Prandtl);
  elseif co_relation == Gasketed_PHX.corelation.Muley_Manglik then
    {f_primary, Nu_primary} = Corelations.Muley_Manglik(Re_primary, beta, splprimaryin.Prandtl, splprimaryin.viscosity, viscosity_avg);
  end if;
  if co_relation == Gasketed_PHX.corelation.Savostin_Tikhonov then
    {f_secondary, Nu_primary} = Corelations.Savostin_Tikhonov(Re_secondary, beta, SEF, splsecondaryin.Prandtl);
  elseif co_relation == Gasketed_PHX.corelation.Tovazhnyanski then
    {f_secondary, Nu_secondary} = Corelations.Tovazhnyanski(Re_secondary, beta, splsecondaryin.Prandtl, Prandtl_avg);
  elseif co_relation == Gasketed_PHX.corelation.Chisholm_Wanniarachchi then
    {f_secondary, Nu_secondary} = Corelations.Chisholm_Wanniarachchi(Re_secondary, beta, SEF, splsecondaryin.Prandtl);
  elseif co_relation == Gasketed_PHX.corelation.Talik then
    {f_secondary, Nu_secondary} = Corelations.Talik(Re_secondary, beta, splsecondaryin.Prandtl);
  elseif co_relation == Gasketed_PHX.corelation.Muley_Manglik then
    {f_secondary, Nu_secondary} = Corelations.Muley_Manglik(Re_secondary, beta, splsecondaryin.Prandtl, splsecondaryin.viscosity, viscosity_avg);
  end if;
  Nu_primary = h_primary * Dh / splprimaryin.Conductivity;
  Nu_secondary = h_secondary * Dh / splsecondaryin.Conductivity;
  1 / U_clean = 1 / h_primary + 1 / h_secondary + t / k;
  1 / U_fouled = 1 / U_clean + Corelations.Fouling.seawater;
  CF = U_fouled / U_clean;
  Qr = min_primary * splprimaryin.CpMass * abs(Tin_primary - Tout_primary);
  Qf = U_fouled * (SEF * Lp * Lw * N_effective) * ((Tin_primary - Tout_secondary) / 2 + (Tout_primary - Tin_secondary) / 2);
  CS = Qf / Qr;
  min_secondary * splsecondaryin.CpMass * (Tin_secondary - Tout_secondary) + min_primary * splprimaryin.CpMass * (Tin_primary - Tout_primary) = secondaryin.d * V * splsecondaryin.CpMass * der(Tout_secondary);
  min_primary * splprimaryin.CpMass * (Tin_primary - Tout_primary) + min_secondary * splsecondaryin.CpMass * (Tin_secondary - Tout_secondary) = primaryin.d * V * splprimaryin.CpMass * der(Tout_primary);
//PRESSURE DROP
  delPc_primary = 4 * f_p * Lv * Np / Dh * (Gc_primary ^ 2 / (2 * primaryin.d)) * (Mu_primary / Mu) ^ (-0.17);
  f_p = Kp / Re_primary ^ m;
  Gc_primary = min_primary / (Ncp * b * Lw);
  Mu_primary = (splprimaryin.viscosity + splprimaryout.viscosity) / 2;
  Mu = (Mu_primary + Mu_secondary) / 2;
  delPc_secondary = 4 * f_s * Lv * Np / Dh * (Gc_secondary ^ 2 / (2 * secondaryin.d)) * (Mu_secondary / Mu) ^ (-0.17);
  f_s = Kp / Re_secondary ^ m;
  Gc_secondary = min_secondary / (Ncp * b * Lw);
  Mu_secondary = (splsecondaryin.viscosity + splsecondaryout.viscosity) / 2;
  delPp_primary = 1.4 * Np * (Gp_primary ^ 2 / (2 * primaryin.d));
  Gp_primary = min_primary / (Modelica.Constants.pi / 4 * Dp ^ 2);
  delPp_secondary = 1.4 * Np * (Gp_secondary ^ 2 / (2 * primaryin.d));
  Gp_secondary = min_secondary / (Modelica.Constants.pi / 4 * Dp ^ 2);
  delP_primary = delPc_primary + delPp_primary;
  delP_secondary = delPc_secondary + delPp_secondary;
  annotation(
    Icon(graphics = {Line(origin = {0, 10}, points = {{100, 90}, {-60, 90}, {-60, -90}, {-100, -90}}, color = {255, 0, 0}, thickness = 0.5), Polygon(origin = {110, 100}, rotation = 90, lineColor = {255, 0, 0}, fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Polygon(origin = {-110, -80}, rotation = 90, lineColor = {255, 0, 0}, fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Line(origin = {0, 10}, points = {{60, 90}, {60, -90}, {20, -90}, {20, 90}, {-20, 90}, {-20, -90}, {-60, -90}}, color = {255, 0, 0}, thickness = 0.5), Polygon(origin = {60, 50}, rotation = 180, lineColor = {255, 0, 0}, fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Polygon(origin = {20, 50}, rotation = 180, lineColor = {255, 0, 0}, fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Polygon(origin = {-20, 50}, rotation = 180, lineColor = {255, 0, 0}, fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Line(origin = {-0.321101, -10}, points = {{-100, -90}, {-80, -90}, {-80, 90}, {100, 90}}, color = {0, 0, 255}, pattern = LinePattern.Dash, thickness = 0.5), Polygon(origin = {-110, -100}, rotation = -90, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, lineThickness = 0.5, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Polygon(origin = {110, 80}, rotation = -90, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, lineThickness = 0.5, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Line(origin = {-20, -10}, points = {{-60, -90}, {-20, -90}, {-20, 90}, {20, 90}, {20, -90}, {60, -90}, {60, 90}, {60, 90}}, color = {0, 0, 255}, pattern = LinePattern.Dash, thickness = 0.5), Polygon(origin = {-80, -50}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, lineThickness = 0.5, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Polygon(origin = {-40, -50}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, lineThickness = 0.5, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Polygon(origin = {0, -50}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, lineThickness = 0.5, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Polygon(origin = {40, -50}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, lineThickness = 0.5, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Polygon(origin = {-60, 50}, rotation = 180, lineColor = {255, 0, 0}, fillColor = {255, 0, 0}, fillPattern = FillPattern.Solid, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Line(origin = {-20, -100}, points = {{-20, 0}, {20, 0}}, color = {0, 0, 255}, pattern = LinePattern.Dash, thickness = 0.5), Line(origin = {60, -10}, points = {{-20, -90}, {20, -90}, {20, 90}}, color = {0, 0, 255}, pattern = LinePattern.Dash, thickness = 0.5), Polygon(origin = {80, -50}, lineColor = {0, 0, 255}, fillColor = {0, 0, 255}, fillPattern = FillPattern.Solid, lineThickness = 0.5, points = {{-10, -10}, {0, 10}, {10, -10}, {-10, -10}}), Line(origin = {0, -80}, points = {{-20, 0}, {20, 0}}, color = {255, 0, 0}, thickness = 0.5)}, coordinateSystem(initialScale = 0.1)));
    end Cooler_Gasketed_PHX;
