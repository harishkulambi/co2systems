within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Corelations;

function Chisholm_Wanniarachchi"Chervon angle = 30-80, Re = 1000-4000"
  extends Modelica.Icons.Function;
  
  input Real Re "Reynolds number";
  input Real beta "Chervon angle";
  input Real SEF "Surface en;argement factor";
  input Real Pr "Prandtl number";
  output Real f[2] "Frictional factor, Nusselt number";
  
algorithm
  
  if((Re>1000 and Re<40000) and (beta>=30 and beta<=80)) then 
    f[1] := 0.08*(Re^(-0.25))*(SEF^1.25)*((beta/30)^3.6);
    f[2] := 0.72*(Re^0.59)*(Pr^0.4)*(SEF^0.41)*((beta/30)^0.66);
  else 
    f[1] := 0;
    f[2] := 0;
  end if;   
end Chisholm_Wanniarachchi;
