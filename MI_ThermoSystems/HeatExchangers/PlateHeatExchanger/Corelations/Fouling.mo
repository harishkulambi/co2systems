within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Corelations;

record Fouling
  extends Modelica.Icons.Record;
    
   constant Real seawater = 0.0000827 "sea water below 52 deg";
   constant Real seawater52 = 0.0017540 "sea water above 52 deg";
   constant Real boilerfeedwater = 0.0001754 "treated boiler feed water above 52 deg";
   constant Real fueloil = 0.000877 "Fuel oil";
   constant Real quenchingoil = 0.0007051 "Quenching oil";
   constant Real alchoalvapors = 0.0000877 "Alchoal vapors";
   constant Real steam = 0.0000877 "Steam not mixed with oil";
   constant Real industrialair = 0.0003525 "Industrial air";
   constant Real refrigerants = 0.0001754 "Refrigerants";
end Fouling;
