within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Corelations;

function Muley_Manglik"Chervon angle = 60"
  extends Modelica.Icons.Function;
    
    input Real Re "Reynolds number";
    input Real beta "Chervon angle";
    input Real Pr "Prandtl number";
    input Real Mu "Dynamic viscosity";
    input Real Muw "Average dynamic viscosity of both fluids";
    output Real f[2] "Frictional factor, Nusselt number";
    
    
algorithm
  
  if((Re<16) and (beta == 60)) then 
    f[1] := 51.5/Re;
  elseif((Re>=16 and Re<=100) and (beta == 60)) then  
    f[1] := 17*(Re^(-0.6));
  elseif((Re>=800) and (beta == 60)) then 
    f[1] := 2.48*(Re^(-0.2));
  end if;
     
  if((Re>=20 and Re<=210) and (beta == 60)) then
    f[2] := 0.572*(Re^(0.5))*(Pr^(1/3))*((Mu/Muw)^0.14);
  elseif((Re>=800) and (beta == 60)) then 
    f[2] := 0.1096*(Re^(0.78))*(Pr^(1/3))*((Mu/Muw)^0.14);    
  end if;  
end Muley_Manglik;
