within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Corelations;

function Savostin_Tikhonov"Air flows,0<=chervon angle<=33"
  extends Modelica.Icons.Function;
  
    input Real Re "Reynolds Number";
    input Real beta "Chervon angle";
    input Real SEF "Surface Enlargement Factor";
    input Real Pr "Prandtl number";
    output Real f[2] "Frictional factor, Nusselt number";
    
 protected 
    
    Real a = Re/SEF;
    Real b = 2*Modelica.SIunits.Conversions.from_deg(beta);
    Real a1 = 0.22*(1+b^1.5);
    Real a2 = 0.53*(0.58+0.42*cos(1.87*b));
       
algorithm

  if((a>=200 and a<=600) and (beta>=0 and beta<=33)) then 
    f[1] := 6.25*(1+0.95*b^1.72)*(SEF^1.84)*(Re^(-0.84));
    f[2] := 1.26*(0.62+0.38*cos(2.3*b))*(SEF^(1-a1))*(Pr^(1/3))*(Re^a1);
    
  elseif((a>600 and a<=4000) and (beta>=0 and beta<=33)) then
    f[1] := 0.925*(0.62+0.38*cos(2.6*b))*((SEF)^(1+a2))*(Re^(-a2));
    f[2] := 0.072*exp((0.5*b)+(0.17*b^2))*(SEF^0.33)*(Pr^(1/3))*(Re^0.67);
  
  end if;  
end Savostin_Tikhonov;
