within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Corelations;

function Talik"Chervon angle = 60"
  extends Modelica.Icons.Function;
  
  input Real Re "Reynolds number";
  input Real beta "Chervon angle";
  input Real Pr "Prandtl number";
  output Real f[2] "Frictional factor, Nusselt number";
  
   
algorithm
  
  if((Re>10 and Re<80) and (beta == 60)) then 
    f[1] := 12.065*(Re^(-0.74));
  elseif((Re>1450 and Re<11460) and (beta == 60)) then 
    f[1] := 0.3323*(Re^(-0.042));
  end if;
  
  if((Re>10 and Re<720) and (beta == 60)) then 
    f[2] := 0.2*((Re^0.75))*(Pr^0.4);
  elseif((Re>1450 and Re<11460) and (beta == 60)) then
    f[2] := 0.248*(Re^0.7)*(Pr^0.4);
  end if;
end Talik;
