within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Corelations;

function Tovazhnyanski"Chervon angle = 30,45,60, Reynolds number = 2000 - 25000"
  extends Modelica.Icons.Function;
  
  input Real Re "Reynolds number";
  input Real beta "Chervon angle";
  input Real Pr "Prandtl number";
  input Real Prw "Avg prandtl number of both fluids";
  output Real f[2] "Frictional factor, Nusselt number";
  
algorithm


  if((Re>200 and Re<25000) and (beta == 30 or beta == 45 or beta == 60))  then 
  
      f[1] := 0.085*exp(1.52*tan(beta))*(Re^(-(0.25 - 0.06*tan(beta))));
      f[2] := 0.051*exp(0.64*tan(beta))*(Re^0.73)*(Pr^0.43)*((Pr/Prw)^0.25);
      
  else 
      f[1] := 0;
      f[2] := 0;
  end if; 
  
end Tovazhnyanski;
