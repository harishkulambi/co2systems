within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger;

model DGPHX_Simulation
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 6.5e+06, Q0 = 2, T0 (displayUnit = "degC") = 393.15)  annotation(
    Placement(visible = true, transformation(origin = {92, 62}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  BoundaryConditions.Source_PQT source_PQT2(P0 = 400000, Q0 = 2, T0 = 298.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-124, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  BoundaryConditions.Sink sink1(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {82, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2 annotation(
    Placement(visible = true, transformation(origin = {-132, 16}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Cooler_Gasketed_PHX cooler_Gasketed_PHX1(A = 50,Tout_primary (displayUnit = "degC") = 323.15,Tout_secondary(start = 288.15),co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide")  annotation(
    Placement(visible = true, transformation(origin = {-11, -7}, extent = {{-31, -31}, {31, 31}}, rotation = 0)));
equation
  connect(sink2.ip, cooler_Gasketed_PHX1.hotout) annotation(
    Line(points = {{-122, 16}, {-52, 16}, {-52, -32}, {-52, -32}}));
  connect(cooler_Gasketed_PHX1.hotin, source_PQT1.op) annotation(
    Line(points = {{30, 24}, {60, 24}, {60, 62}, {82, 62}, {82, 62}}));
  connect(cooler_Gasketed_PHX1.coldout, sink1.ip) annotation(
    Line(points = {{30, 18}, {72, 18}, {72, 22}, {72, 22}}));
  connect(source_PQT2.op, cooler_Gasketed_PHX1.coldin) annotation(
    Line(points = {{-114, -48}, {-52, -48}, {-52, -38}, {-52, -38}}));
end DGPHX_Simulation;
