within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger;

model PHX_delP_simulation
extends Modelica.Icons.Example;
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.PHX_delP pHX_delP1(co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski)  annotation(
    Placement(visible = true, transformation(origin = {3, 3}, extent = {{-37, -37}, {37, 37}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 140, T0 = 338.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {86, 70}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-90, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PT source_PT1(P0 = 400000, T0 = 295.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-86, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation


  connect(source_PT1.op, pHX_delP1.coldin) annotation(
    Line(points = {{-76, -70}, {-60, -70}, {-60, -34}, {-45, -34}}));
  connect(sink2.ip, pHX_delP1.hotout) annotation(
    Line(points = {{-78, 0}, {-60, 0}, {-60, -27}, {-45, -27}}));
  connect(sink1.ip, pHX_delP1.coldout) annotation(
    Line(points = {{79, 0}, {60, 0}, {60, 33}, {51, 33}}));
  connect(source_PQT1.op, pHX_delP1.hotin) annotation(
    Line(points = {{74, 70}, {60, 70}, {60, 40}, {51, 40}}));
end PHX_delP_simulation;
