within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger;

model SeriesParallel_Without_MassFlowRate
  extends Modelica.Icons.Example;
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX gasketed_PHX1(co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski) annotation(
    Placement(visible = true, transformation(origin = {-90.5, 81.5}, extent = {{-28.5, -28.5}, {28.5, 28.5}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX gasketed_PHX2(co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski) annotation(
    Placement(visible = true, transformation(origin = {90.5, 81.5}, extent = {{-28.5, 28.5}, {28.5, -28.5}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX gasketed_PHX3(co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski) annotation(
    Placement(visible = true, transformation(origin = {-90.5, -81.5}, extent = {{-28.5, 28.5}, {28.5, -28.5}}, rotation = 0)));
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX gasketed_PHX4(co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski) annotation(
    Placement(visible = true, transformation(origin = {90.5, -79.5}, extent = {{-28.5, -28.5}, {28.5, 28.5}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.Merger merger1(fluid = "water") annotation(
    Placement(visible = true, transformation(origin = {210, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.Components.Tanks.Merger merger2(fluid = "water") annotation(
    Placement(visible = true, transformation(origin = {-210, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_splitter source_splitter1 annotation(
    Placement(visible = true, transformation(origin = {-150, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_splitter source_splitter2 annotation(
    Placement(visible = true, transformation(origin = {150, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1(fluid = "water") annotation(
    Placement(visible = true, transformation(origin = {-240, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2(fluid = "water") annotation(
    Placement(visible = true, transformation(origin = {240, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PT source_PT1(P0 = 400000, T0 = 306.15, fluid = "water") annotation(
    Placement(visible = true, transformation(origin = {-180, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 280, T0 = 373.15, fluid = "water") annotation(
    Placement(visible = true, transformation(origin = {180, 0}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
equation
  connect(source_PQT1.op, source_splitter2.ip) annotation(
    Line(points = {{169, 0}, {160, 0}, {160, -1}, {161, -1}, {161, 0}}));
  gasketed_PHX1.delP_primary + gasketed_PHX2.delP_primary = gasketed_PHX1.delP_secondary + gasketed_PHX2.delP_secondary;
  gasketed_PHX3.delP_primary + gasketed_PHX4.delP_primary = gasketed_PHX3.delP_secondary + gasketed_PHX4.delP_secondary;
(gasketed_PHX2.delP_primary + gasketed_PHX1.delP_primary) =(gasketed_PHX3.delP_primary + gasketed_PHX4.delP_primary);
//-embeddedServer opc-ua -rt 1.0
  connect(merger1.op, sink2.ip) annotation(
    Line(points = {{221, 0}, {229, 0}, {229, 0}, {229, 0}}));
  connect(merger1.ipL, gasketed_PHX4.coldout) annotation(
    Line(points = {{199, -7}, {200, -7}, {200, -57}, {128, -57}, {128, -57}}));
  connect(merger1.ipV, gasketed_PHX2.coldout) annotation(
    Line(points = {{199, 7}, {200, 7}, {200, 59}, {128, 59}, {128, 59}}));
  connect(source_splitter2.op1, gasketed_PHX4.hotin) annotation(
    Line(points = {{139, -7}, {140, -7}, {140, -52}, {128, -52}, {128, -51}}));
  connect(source_splitter2.op, gasketed_PHX2.hotin) annotation(
    Line(points = {{139, 7}, {140, 7}, {140, 53}, {128, 53}, {128, 53}}));
  connect(merger2.op, sink1.ip) annotation(
    Line(points = {{-221, 0}, {-229, 0}, {-229, 0}, {-229, 0}}));
  connect(gasketed_PHX3.hotout, merger2.ipL) annotation(
    Line(points = {{-128, -59}, {-200, -59}, {-200, -7}, {-199, -7}}));
  connect(gasketed_PHX1.hotout, merger2.ipV) annotation(
    Line(points = {{-128, 59}, {-199, 59}, {-199, 7}, {-199, 7}}));
  connect(source_PT1.op, source_splitter1.ip) annotation(
    Line(points = {{-169, 0}, {-161, 0}, {-161, 0}, {-161, 0}}));
  connect(source_splitter1.op1, gasketed_PHX3.coldin) annotation(
    Line(points = {{-139, -7}, {-140, -7}, {-140, -54}, {-128, -54}, {-128, -53}}));
  connect(source_splitter1.op, gasketed_PHX1.coldin) annotation(
    Line(points = {{-139, 7}, {-140, 7}, {-140, 52}, {-128, 52}, {-128, 53}}));
  connect(gasketed_PHX4.hotout, gasketed_PHX3.hotin) annotation(
    Line(points = {{53, -102}, {-13, -102}, {-13, -113}, {-26, -113}, {-26, -110}, {-53, -110}, {-53, -110}}));
  connect(gasketed_PHX3.coldout, gasketed_PHX4.coldin) annotation(
    Line(points = {{-53, -104}, {-21, -104}, {-21, -109}, {53, -109}, {53, -108}}));
  connect(gasketed_PHX2.hotout, gasketed_PHX1.hotin) annotation(
    Line(points = {{53, 104}, {-15, 104}, {-15, 100}, {-29, 100}, {-29, 110}, {-53, 110}, {-53, 110}}));
  connect(gasketed_PHX1.coldout, gasketed_PHX2.coldin) annotation(
    Line(points = {{-53, 104}, {-20, 104}, {-20, 110}, {53, 110}, {53, 110}}));
  annotation(
    Diagram(coordinateSystem(extent = {{-250, -150}, {250, 150}}, initialScale = 0.3, grid = {1, 1})),
    Icon(coordinateSystem(extent = {{-250, -150}, {250, 150}}, initialScale = 0.3, grid = {1, 1})),
    __OpenModelica_commandLineOptions = "");
end SeriesParallel_Without_MassFlowRate;
