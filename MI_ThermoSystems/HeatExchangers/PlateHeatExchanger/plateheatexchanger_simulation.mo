within MI_ThermoSystems.HeatExchangers.PlateHeatExchanger;

model plateheatexchanger_simulation
extends Modelica.Icons.Example;
  MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX gasketed_PHX1(   A = 50,Tout_primary(start = 293.15), Tout_secondary(start = 323.15),V = 1,co_relation = MI_ThermoSystems.HeatExchangers.PlateHeatExchanger.Gasketed_PHX.corelation.Tovazhnyanski, fluid1 = "carbondioxide", fluid2 = "water")  annotation(
    Placement(visible = true, transformation(origin = {0,8}, extent = {{-38, -38}, {38, 38}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Source_PQT source_PQT2(P0 = 7.5e+06, Q0 = 2, T0 = 443.15)  annotation(
    Placement(visible = true, transformation(origin = {90, 62}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink1 annotation(
    Placement(visible = true, transformation(origin = {-90, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  MI_ThermoSystems.BoundaryConditions.Sink sink2(fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {90, 30}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  BoundaryConditions.Source_PQT source_PQT1(P0 = 400000, Q0 = 2, T0 = 298.15, fluid = "water")  annotation(
    Placement(visible = true, transformation(origin = {-90, -46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(source_PQT1.op, gasketed_PHX1.coldin) annotation(
    Line(points = {{-78, -46}, {-66, -46}, {-66, -30}, {-50, -30}, {-50, -30}}));
  connect(gasketed_PHX1.hotin, source_PQT2.op) annotation(
    Line(points = {{50, 46}, {64, 46}, {64, 62}, {78, 62}, {78, 62}}));
  connect(sink1.ip, gasketed_PHX1.hotout) annotation(
    Line(points = {{-80, -10}, {-66, -10}, {-66, -22}, {-50, -22}, {-50, -22}}));
  connect(gasketed_PHX1.coldout, sink2.ip) annotation(
    Line(points = {{50, 38}, {64, 38}, {64, 30}, {80, 30}, {80, 30}}));
end plateheatexchanger_simulation;
