within MI_ThermoSystems.HeatExchangers;

model recuperator
  
  import SI = Modelica.SIunits;
  
  parameter String fluid1 = "carbondioxide";
  parameter String fluid2 = "carbondioxide";
  
  CoolPropLinker.Interfaces.Functions CP;
  
  CoolPropLinker.Interfaces.BaseProperties primaryin;
  CoolPropLinker.Interfaces.BaseProperties primaryout;
  CoolPropLinker.Interfaces.BaseProperties secondaryin;
  CoolPropLinker.Interfaces.BaseProperties secondaryout;
  
  CoolPropLinker.Interfaces.SpecialProperties splprimaryin;
  CoolPropLinker.Interfaces.SpecialProperties splprimaryout;
  CoolPropLinker.Interfaces.SpecialProperties splsecondaryin;
  CoolPropLinker.Interfaces.SpecialProperties splsecondaryout;
  
  parameter SI.Length Lv = 1.55;
  parameter SI.Length Lh = 0.43;
  parameter SI.Length Lc = 2;
  parameter SI.Thickness t = 0.6e-3;
  parameter SI.Area A = 187;
  parameter Real SEF = 1.25;
  parameter SI.Diameter Dp = 200e-3;
  parameter SI.Volume V = 1;
  
  SI.Area A_projected;
  SI.Area A_developed;
  SI.Area Ach;
  
  SI.Length Lp;
  SI.Length Lw;
  
  Real Neffective;
  Real Nt;
  Real Ncp;
  parameter Real Np = 1;
  SI.Diameter Dh;
  Real b;
  Real p;

// hot fluid
    parameter SI.Pressure Pin_primary = 75e5;
    parameter SI.Temperature Tin_primary = 476+273.15;
    parameter SI.MassFlowRate min_primary = 2;
    
    SI.SpecificEnthalpy hin_primary,hout_primary;
    
    SI.Temperature Tout_primary(start = 400+273.15);
//cold side
    parameter SI.Pressure Pin_secondary = 120e5;
    parameter SI.Temperature Tin_secondary = 120+273.15;
    parameter SI.MassFlowRate min_secondary = 2;
    
    SI.SpecificEnthalpy hin_secondary,hout_secondary;
    
    SI.Temperature Tout_secondary(start = 400+273.15); 
//heat transfer data

    SI.MassFlowRate mch_primary;
    SI.MassFlowRate mch_secondary;
    
    Real G_primary;
    Real G_secondary;
    
    Real Re_primary;
    Real Re_secondary;
    Real Pr;
    Real Mu;
    Real Nu_primary,Nu_secondary;
    Real h_primary,h_secondary;
    Real U,Qr,Qf;
           
equation
  
  SEF = A_developed/A_projected;
      
      A_projected = Lp*Lw;
        Lp = Lv - Dp;
        Lw = Lh + Dp;
  A_developed = A/Neffective;
  
  Nt = ceil(Neffective)+2;
  Ncp = (Nt - 1)/(2*Np);
  
  b = p - t;
    p = Lc/Nt;
  
  Dh = 2*b/SEF;
  Ach = b*Lw;         

//fluid properties

    primaryin.state = CP.setProperty(Pin_primary,Tin_primary,fluid1,"pT");
    primaryin.h = hin_primary;
    
    splprimaryin.state = CP.setProperty(Pin_primary,hin_primary,fluid1,"SpecialProperties");
    
    primaryout.state = CP.setProperty(Pin_primary,Tout_primary,fluid1,"pT");
    primaryout.h = hout_primary;
    
    splprimaryout.state = CP.setProperty(Pin_primary,hout_primary,fluid1,"SpecialProperties");
    
    secondaryin.state = CP.setProperty(Pin_secondary,Tin_secondary,fluid2,"pT");
    secondaryin.h = hin_secondary;
    
    splsecondaryin.state = CP.setProperty(Pin_secondary,hin_secondary,fluid2,"SpecialProperties");
    
    secondaryout.state = CP.setProperty(Pin_secondary,Tout_secondary,fluid2,"pT");
    secondaryout.h = hout_secondary;
    
    splsecondaryout.state = CP.setProperty(Pin_secondary,hout_secondary,fluid2,"SpecialProperties");
    
    mch_primary = min_primary/ceil(Ncp);
    mch_secondary = min_secondary/ceil(Ncp);
    
    G_primary = mch_primary/Ach;
    G_secondary = mch_secondary/Ach;
    
    Re_primary = G_primary*Dh/splprimaryin.viscosity;
    Re_secondary = G_secondary*Dh/splsecondaryin.viscosity;
    
    Pr = (splprimaryin.Prandtl+splsecondaryin.Prandtl)/2;
    Mu = (splprimaryin.viscosity+splsecondaryin.viscosity)/2;
    
    Nu_primary = 0.3*(Re_primary)^0.663*(splprimaryin.Prandtl)^(1/3);
    Nu_secondary = 0.3*(Re_secondary)^0.663*(splsecondaryin.Prandtl)^(1/3);
    
    Nu_primary = h_primary*Dh/splprimaryin.Conductivity;
    Nu_secondary = h_secondary*Dh/splsecondaryin.Conductivity;
    
    (1/U) = (1/h_primary)+(1/h_secondary)+(t/17.5)+0.0001754;
    
    Qr = min_primary*splprimaryin.CpMass*abs(Tin_primary-Tout_primary);
//     Qr = min_secondary*splsecondaryin.CpMass*abs(Tin_secondary-Tout_secondary);
    Qf = U*(SEF*Lp*Lw*Neffective)*(((Tin_primary-Tout_secondary)/2)+(Tout_primary-Tin_secondary)/2);
    
//    min_secondary*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary) + min_primary*splprimaryin.CpMass*(Tin_primary - Tout_primary) = secondaryin.d*V*splsecondaryin.CpMass*der(Tout_secondary);
    
//    min_primary*splprimaryin.CpMass*(Tin_primary-Tout_primary) + min_secondary*splsecondaryin.CpMass*(Tin_secondary-Tout_secondary) = primaryin.d*V*splprimaryin.CpMass*der(Tout_primary);
        
    
//    -min_secondary*splsecondaryin.CpMass*(Tout_secondary-Tin_secondary) - min_primary*splprimaryin.CpMass*(Tout_primary - Tin_primary) = secondaryin.d*V*splsecondaryin.CpMass*der(Tout_primary);
    
//    -min_primary*splprimaryin.CpMass*(Tout_primary-Tin_primary) - min_secondary*splsecondaryin.CpMass*(Tout_secondary-Tin_secondary) = primaryin.d*V*splprimaryin.CpMass*der(Tout_secondary);

  -min_secondary*splsecondaryin.CpMass*(Tout_secondary-Tin_secondary) + min_primary*splprimaryin.CpMass*(Tin_primary - Tout_primary) = secondaryin.d*V*splsecondaryin.CpMass*der(Tout_secondary);
    
    min_primary*splprimaryin.CpMass*(Tin_primary-Tout_primary) - min_secondary*splsecondaryin.CpMass*(Tout_secondary-Tin_secondary) = primaryin.d*V*splprimaryin.CpMass*der(Tout_primary);
    
end recuperator;
