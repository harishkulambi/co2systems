within MI_ThermoSystems.Interfaces;

connector InletPort"Connector at Inlet"

  extends Port;
    input Boolean a = true "Orientation Check";
    output Boolean b "Orientation Check";
    annotation(defaultComponentName = "ip",
    Icon(graphics = {Rectangle(fillColor = {170, 170, 255}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}})}));

end InletPort;
