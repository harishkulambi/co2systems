within MI_ThermoSystems.Interfaces;

connector OutletPort

  extends Port;
  
    output Boolean a "Orientation Check";
    input Boolean b = true "Orientation Check";
    annotation(defaultComponentName = "op",
    Icon(graphics = {Rectangle(fillColor = {0, 255, 0}, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}})}));
  
end OutletPort;
