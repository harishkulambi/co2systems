within MI_ThermoSystems.Interfaces;

connector Port "Basic Connector Port"

  Modelica.SIunits.Pressure P(start = 10e5) "Pressure";
  Modelica.SIunits.MassFlowRate Q(start = 100) "Mass Flow Rate";
  Modelica.SIunits.SpecificEnthalpy h(start = 4e5) "Specific Enthalpy";
  
  annotation(Icon(graphics = {Rectangle(extent = {{-100, 100}, {100, -100}})}));

end Port;
