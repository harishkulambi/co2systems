within MI_ThermoSystems.UserGuide;

package Overview
  extends Modelica.Icons.Information;
  annotation(
    Documentation(info = "<html><head></head><body><div><br></div><div>This package contains the thermodynamic models of all the thermal models such as compressors, valves, pipes, heat exchangers etc. These models are valid for both steady state and dynamic simulation. The components created are object oriented which can be used any number of times by drag and drop and connecting it to other components and test various cycle configurations. The various cycles for different fluids can be simulated with the help of CoolProp package built along with MI_Thermosystems. The CoolProp library will calculate the properties of the fluid at given conditions.</div><div><br></div></body></html>"));
end Overview;
