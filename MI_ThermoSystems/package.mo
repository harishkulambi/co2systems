package MI_ThermoSystems
  extends Modelica.Icons.Package;






  annotation(
    uses(Modelica(version = "3.2.3")),
    Documentation(info = "<html><head></head><body><p style=\"font-size: 12px;\"><strong><u>Information</u></strong></p><div class=\"textDoc\"><p style=\"font-family: 'Courier New'; font-size: 12px;\"></p></div><div class=\"htmlDoc\"><p><b>Copyright © ModeliCon InfoTech 2018</b></p><p><b>Functions to link CoolProp to OM</b></p><dl><dt><b>Main Author:</b></dt><dd>Harish M Y<br>Systems Engineer&nbsp;<br>ModeliCon InfoTech, Bangalore<br>email:&nbsp;<a href=\"mailto:info@modelicon.in\">info@modelicon.in</a><br></dd></dl></div><div class=\"textDoc\"><p style=\"font-family: 'Courier New'; font-size: 12px;\"></p></div><p style=\"font-size: 12px;\"><strong><u>Revisions</u></strong></p><div class=\"textDoc\"><p style=\"font-family: 'Courier New'; font-size: 12px;\"></p></div><div class=\"htmlDoc\"><div class=\"textDoc\"><p style=\"font-family: 'Courier New'; font-size: 12px;\"></p></div><table border=\"1\" cellspacing=\"0\" cellpadding=\"2\"><tbody><tr><th>Version</th><th>Date</th><th>Authors</th><th>Description</th></tr><tr><td valign=\"top\">1.0</td><td valign=\"top\">01/08/2019</td><td valign=\"top\">Harish M Y</td><td valign=\"top\">Created the package</td></tr></tbody></table></div></body></html>"));
end MI_ThermoSystems;
